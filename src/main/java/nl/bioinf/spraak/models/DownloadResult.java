package nl.bioinf.spraak.models;

import java.util.List;

public class DownloadResult {

    private int id;
    private String session;
    private String timeStamp;
    private int fileAmount;
    private String owner;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getFileAmount() {
        return fileAmount;
    }

    public void setFileAmount(int fileAmount) {
        this.fileAmount = fileAmount;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public DownloadResult(int id, String session, String timeStamp, int fileAmount, String owner) {
        this.id = id;
        this.session = session;
        this.timeStamp = timeStamp;
        this.fileAmount = fileAmount;
        this.owner = owner;
    }

    public DownloadResult() {
    }

}
