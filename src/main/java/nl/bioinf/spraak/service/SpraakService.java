package nl.bioinf.spraak.service;

/**
 *  created by Marcel Zandberg
 */

import nl.bioinf.spraak.data.DataSource;
import nl.bioinf.spraak.models.SpraakResults;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Spraak service.
 */
@Service
public class SpraakService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);


    private final DataSource dataSource;

    /**
     * The Api service.
     */
    @Autowired
    APIService apiService;

    /**
     * Instantiates a new Spraak service.
     *
     * @param dataSource the data source
     */
    @Autowired
    public SpraakService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Insert praat results string.
     *
     * @param spraakResults   the spraak results
     * @param principal       the principal
     * @param usePartMatching the use part matching
     * @param auth            the auth
     * @param url             the url
     * @return the string
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public String insertPraatResults(SpraakResults spraakResults, Principal principal, boolean usePartMatching, Authentication auth, String url) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "praatservice insertpraatresults executed");

            List<NameValuePair> arguments = new ArrayList<>(1);
            arguments.add(new BasicNameValuePair("username", principal.getName()));


            return apiService.storePraatResults(auth, principal, url, arguments, spraakResults);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }


    /**
     * Inser praat max upload int.
     *
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int inserPraatMaxUpload(boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {

            return dataSource.insertPraatMaxUpload();
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }


}

