package nl.bioinf.spraak.service;

/**
 *  created by Marcel Zandberg
 *  Extended during internship by Bas Kasemir
 */

import nl.bioinf.spraak.data.DataSource;
import nl.bioinf.spraak.models.LoginAttempts;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.models.UserNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type User service.
 */
@Service
public class UserService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final DataSource dataSource;

    /**
     * Instantiates a new User service.
     *
     * @param dataSource the data source
     */
    @Autowired
    public UserService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Gets user by user name.
     *
     * @param userName        the user name
     * @param usePartMatching the use part matching
     * @return the user by user name
     */
    public User getUserByUserName(String userName, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService get user by username executed for user "+userName);
            return dataSource.getUserByUserName(userName);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Gets user by user name.
     *
     * @param username        the user name
     * @param usePartMatching the use part matching
     * @return the user by user name
     */
    public User getUserDataForLogin(String username, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService get user login info executed for "+username);
            return dataSource.getUserDataForLogin(username);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Gets user by user name.
     *
     * @param userName        the user name
     * @param usePartMatching the use part matching
     * @return the user by user name
     */
    public LoginAttempts getLoginAttemptsByUsername(String userName, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService get Loginattempts executed for user "+userName);
            return dataSource.getLoginAttemptsByUsername(userName);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update Account enabled.
     *
     * @param user    the user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updateAccountActivation(User user, boolean activated,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService update Account activation executed for user "+user.getUserName()+", value="+activated);
            return dataSource.updateAccountActivation(user, activated);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update password token
     *
     * @param user    the user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updatePasswordReset(String user, String datetime,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Update password reset executed for user "+user);
            return dataSource.updatePasswordReset(user, datetime);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update password token
     *
     * @param user    the user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updatePasswordLinkValid(String user, boolean bool,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "Update password link valid executed for user "+user);
            return dataSource.updatePasswordLinkValid(user, bool);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update password token
     *
     * @param user    the user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updatePasswordToken(User user, String token,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService update password token executed for user "+user.getUserName());
            return dataSource.updatePasswordToken(user, token);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update password
     *
     * @param user    the user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updatePassword(User user, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService update password executed for user "+user.getUserName());
            return dataSource.updatePassword(user);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Gets all users.
     *
     * @param userName        the user name
     * @param usePartMatching the use part matching
     * @return the all users
     */
    public List<User> getAllUsers(String userName, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice get all user info executed");
            return dataSource.getAllUsers(userName);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Gets user by password.
     *
     * @param password        the password
     * @param usePartMatching the use part matching
     * @return the user by password
     */
    public User getUserByPassword(String password, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice get user by password executed");
            return dataSource.getUserByUserName(password);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Insert user int.
     *
     * @param registerUser    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertUser(UserNew registerUser, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice insertuser executed");
            return dataSource.insertUser(registerUser);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Delete user int.
     *
     * @param username    the user to delete
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int deleteUserUserTable(String username, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice DELETE from USER TABLE executed");
            return dataSource.deleteUserFromUserTable(username);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Delete user int.
     *
     * @param username    the user to delete
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int deleteUserRoleTable(String username, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice DELETE from ROLE TABLE executed");
            return dataSource.deleteUserFromRoleTable(username);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Delete user int.
     *
     * @param username    the user to delete
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int deleteUserLATable(String username, boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice DELETE from Login Attempts TABLE executed");
            return dataSource.deleteUserFromLATable(username);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update user int.
     *
     * @param registerUser    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updateUser(UserNew registerUser,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice update user executed");
            return dataSource.updateUser(registerUser);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update login attempts.
     *
     * @param loginAttempts    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updateLoginAttempts(LoginAttempts loginAttempts,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "UserService update Account activation executed for user "+loginAttempts.getUsername()+", value="+loginAttempts.getNumber_of_wrong_attempts());
            return dataSource.updateLoginAttempts(loginAttempts);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Add to login attempts.
     *
     * @param registerUser    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int addLoginAttempts(UserNew registerUser,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "addLoginAttempts executed for user " + registerUser.getUserName());
            return dataSource.addLoginAttempts(registerUser);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Insert role int.
     *
     * @param registerUser    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertRole(UserNew registerUser,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice insert role executed");
            return dataSource.insertRole(registerUser);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    /**
     * Update role int.
     *
     * @param registerUser    the register user
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int updateRole(UserNew registerUser,  boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice update role executed");
            return dataSource.updateRole(registerUser);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    public int getNumberOfUsernames(String userName, boolean usePartMatching) {
        if (!usePartMatching) {
            logger.log(Level.INFO, "userservice get number of usernames executed");
            return dataSource.getNumberOfUsernames(userName);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
}
