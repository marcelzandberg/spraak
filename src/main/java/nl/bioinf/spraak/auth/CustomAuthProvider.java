package nl.bioinf.spraak.auth;

import nl.bioinf.spraak.models.LoginAttempts;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.EmailService;
import nl.bioinf.spraak.service.UserService;
import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

@Configuration
@PropertySource(value={"classpath:application.properties","classpath:locale/messages.properties","classpath:locale/messages_nl.properties"})
@Component
public class CustomAuthProvider implements AuthenticationProvider {

    @Value("${application.host}")
    String appHost;

    @Value("${mail.disabled.subject.en}")
    String mail_disabled_subject_en;

    @Value("${mail.disabled.en}")
    String mail_disabled_en;

    @Value("${mail.reactivate.en}")
    String mail_reactivate_en;

    @Value("${mail.disabled.subject.nl}")
    String mail_disabled_subject_nl;

    @Value("${mail.disabled.nl}")
    String mail_disabled_nl;

    @Value("${mail.reactivate.nl}")
    String mail_reactivate_nl;

    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;

    @Autowired
    private EmailService emailService;

    public CustomAuthProvider(UserService userService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String name = authentication.getName().toLowerCase();
        String password = authentication.getCredentials().toString();

        User user = null;

        // Check if the user exists
        try {
            user = userService.getUserDataForLogin(name, false);
        } catch (EmptyResultDataAccessException exception) {
            throw new BadCredentialsException("Invalid");
        }

        // Set a variable containg the 2fa code
        String verificationCode
                = ((CustomWebAuthenticationDetails) authentication.getDetails())
                .getVerificationCode();

        if ((user == null)) {
            logger.log(Level.WARNING, "Login failed for user "+name+", User obj empty");
            throw new BadCredentialsException("Invalid username or password");
        }

        // Get the login attempts model for the user
        LoginAttempts loginAttempts = userService.getLoginAttemptsByUsername(name, false);

        if (user.getEnabled() == 0) {
            logger.log(Level.WARNING, "Login failed for user "+name+", account not activated");
            throw new AccountExpiredException("Account is not enabled");
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12); // Strength set as 16
        // Check if the passwords match, if not throw exception
        if (!encoder.matches(password, user.getPassword())) {
            int attempts = loginAttempts.getNumber_of_wrong_attempts();
            int new_attempts = attempts + 1;
            loginAttempts.setNumber_of_wrong_attempts(new_attempts);

            setNewNumberOfAttempts(loginAttempts, new_attempts);

            if (new_attempts == 3) {
                disableAccount(name);
            }
            logger.log(Level.WARNING, "Login failed for user "+name+", password incorrect");
            throw new BadCredentialsException("Invalid username or password");
        }

        if (verificationCode.equals("")) {
            logger.log(Level.WARNING, "Login failed for user "+name+", 2FA code is empty");
            throw new BadCredentialsException("Missing forms");
        }

        Totp totp = new Totp(user.getTotp_secret());

        // Check if the code is a valid type long and if the code is valid, if not throw an badcredentials exception
        if (!isValidLong(verificationCode) || !totp.verify(verificationCode)) {
            // Check if the code is valid, otherwise we have to update the attempts before throwing a Bad Credentials exception
            if (!totp.verify(verificationCode)) {
                int attempts = loginAttempts.getNumber_of_wrong_attempts();
                int new_attempts = attempts + 1;
                loginAttempts.setNumber_of_wrong_attempts(new_attempts);

                if (new_attempts == 3) {
                    disableAccount(name);
                }

                try {
                    userService.updateLoginAttempts(loginAttempts, false);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            throw new BadCredentialsException("Invalid verfication code"); }

        // If the program gets here, everything is allright so we can set the number of attempts to zero
        setNewNumberOfAttempts(loginAttempts, 0);

        // Finally add the role to the authentication object
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));

        logger.log(Level.INFO, "Login successful for user "+name);

        // return the auth object
        return new UsernamePasswordAuthenticationToken(
                name, password, grantedAuthorities);
    }

    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private void setNewNumberOfAttempts(LoginAttempts loginAttempts, int amount) {
        loginAttempts.setNumber_of_wrong_attempts(amount);

        // Update the database
        try {
            userService.updateLoginAttempts(loginAttempts, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void disableAccount(String username) {

        User user = userService.getUserByUserName(username, false);

        String token = Base32.random();
        // Set the account to disabled to prevent furter logins

        try {
            userService.updateAccountActivation(user, false, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            userService.updatePasswordToken(user, token, false);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String reactURL = "http://"+appHost+"/user/activate?username="+user.getUserName()+"&token="+token;

        String content;
        String subject;

        if (user.getProfile_language().equals("nl-NL")) {
            content = "Hi "+user.getFirst_name()+",<br>" +
                    mail_disabled_nl+"<br>" +
                    mail_reactivate_nl;
            subject = mail_disabled_subject_nl;
        } else {
            content = "Hi "+user.getFirst_name()+",<br>" +
                    mail_disabled_en+"<br>" +
                    mail_reactivate_en;
            subject = mail_disabled_subject_en;
        }

        String emailGeneral = "<br><br>" +
                "<a href=\""+reactURL+"\">"+reactURL+"</a>" +
                "<br><br>--<br>Copyright 2020 UMC Utrecht, Universitair Medisch Centrum Groningen<br><br>" +
                "UMC Utrecht<br>" +
                "Heidelberglaan 100<br>" +
                "3584 CX Utrecht<br><br>"+
                "Universitair Medisch Centrum Groningen<br>" +
                "Hanzeplein 1<br>" +
                "9713 GZ Groningen";

        content += emailGeneral;

        ExecutorService service = Executors.newFixedThreadPool(4);
        String finalContent = content;
        /*service.submit(new Runnable() {
            public void run() {
                try {
                    emailService.sendmail(subject, finalContent, "contentTYpe", user.getEmail());
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });*/

    }

}

