// Copyright 2019 Bas Kasemir

var httpobj;

$( document ).ready( function () {

    getAll();

    $("#refresh-api-status").click( function () {
        getAll();
    });

    $("#view-amt-http").change(function () {
        renderHTTPTrace(httpobj);
    });

});

function getAll() {
    getAPIStatus();
    getAPIEnv();
    getAPIMem();
    getAPICPU();
    getAPIHTTPTrace();
}

function getAPIStatus() {

    var url = encodeURIComponent("/actuator/health");
    $.get("/api/get?url=" + url).done(function (data) {
        $("#timestamp").text(d.toLocaleString());
        renderDiskSpace(data);
        renderState(data);
    });

}

function getAPIMem() {

    var url = encodeURIComponent("/actuator/metrics/jvm.memory.used");
    $.get("/api/get?url=" + url).done(function (data) {
        $("#timestamp").text(d.toLocaleString());
        var usedMem = data["measurements"][0]["value"];
        var url = encodeURIComponent("/actuator/metrics/jvm.memory.max");
        $.get("/api/get?url=" + url).done(function (data) {
            $("#timestamp").text(d.toLocaleString());
            var maxMem = data["measurements"][0]["value"];
            renderMem(usedMem, maxMem);
        });
    });

}

function getAPICPU() {

    var url = encodeURIComponent("/actuator/metrics/system.cpu.usage");
    $.get("/api/get?url=" + url).done(function (data) {
        $("#timestamp").text(d.toLocaleString());
        var cpuLoad = data["measurements"][0]["value"];
        renderCPU(cpuLoad);
    });

}

function getAPIEnv() {

    var url = encodeURIComponent("/actuator/env");
    $.get("/api/get?url=" + url).done(function (data) {
        $("#timestamp").text(d.toLocaleString());
        renderEnv(data);
    });

}

function getAPIHTTPTrace() {

    var url = encodeURIComponent("/actuator/httptrace");
    $.get("/api/get?url=" + url).done(function (data) {
        $("#timestamp").text(d.toLocaleString());
        httpobj = data;
        renderHTTPTrace();
    });

}

function renderState(data) {

    var state = data["status"];
    var dbstate = data["details"]["db"]["status"];

    if (state == "UP") {
        var state_iconclass = "fas fa-check-circle download_green_text";
    } else {
        var state_iconclass = "fas fa-times-circle dogwood_blossom_red_text";
    }

    if (dbstate == "UP") {
        var dbstate_iconclass = "fas fa-check-circle download_green_text";
    } else {
        var dbstate_iconclass = "fas fa-times-circle dogwood_blossom_red_text";
    }

    $("#overall-state").html('<i class="'+state_iconclass+'"></i>&nbsp;'+state);
    $("#db-state").html('<i class="'+dbstate_iconclass+'"></i>&nbsp;'+dbstate);

}

function renderDiskSpace(data) {

    var max_space = (Number(data["details"]["diskSpace"]["details"]["total"])*0.000000001).toFixed(2);
    var used_space = (max_space - Number(data["details"]["diskSpace"]["details"]["free"])*0.000000001).toFixed(2);

    var percentage = used_space / max_space;
    if (percentage > 0.9) {
        $("#Diskspace").next().removeClass("hideonload");
    }

    var canvas = document.getElementById("Diskspace");
    var ctx = canvas.getContext("2d");

    ctx.strokeStyle = spraak_colors.thundercloud_grey;
    ctx.beginPath();
    ctx.lineWidth = 15;
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + 2) * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.strokeStyle = spraak_colors.royal_blue; //red
    ctx.lineWidth = 15;
    ctx.lineCap = "round";
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + (percentage * 2)) * Math.PI);
    ctx.stroke();

    $("#disk_total").text(max_space);
    $("#disk_free").text(used_space);

}

function renderMem(usedMem, maxMem) {

    var maxMemGB = (Number(maxMem)*0.000000001).toFixed(2);
    var usedMemGB = (Number(usedMem)*0.000000001).toFixed(2);

    var canvasID = "Memory";

    var percentage = usedMem / maxMem;
    if (percentage > 0.9) {
        $("#"+canvasID).next().removeClass("hideonload");
    }

    var canvas = document.getElementById(canvasID);
    var ctx = canvas.getContext("2d");

    ctx.strokeStyle = spraak_colors.thundercloud_grey;
    ctx.beginPath();
    ctx.lineWidth = 15;
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + 2) * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.strokeStyle = spraak_colors.royal_blue;
    ctx.lineWidth = 15;
    ctx.lineCap = "round";
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + (percentage * 2)) * Math.PI);
    ctx.stroke();

    $("#MaxMem").text(maxMemGB);
    $("#UsedMem").text(usedMemGB);

}

function renderCPU(cpuLoad) {

    var canvasID = "CPU";

    if (cpuLoad > 0.9) {
        $("#"+canvasID).next().removeClass("hideonload");
    }

    var canvas = document.getElementById(canvasID);
    var ctx = canvas.getContext("2d");

    ctx.strokeStyle = spraak_colors.thundercloud_grey;
    ctx.beginPath();
    ctx.lineWidth = 15;
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + 2) * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.strokeStyle = spraak_colors.royal_blue;
    ctx.lineWidth = 15;
    ctx.lineCap = "round";
    ctx.arc(100, 100, 85, 1.5 * Math.PI, (1.5 + (cpuLoad * 2)) * Math.PI);
    ctx.stroke();

    $("#CPULoad").text(Math.floor(cpuLoad*100));

}

function renderHTTPTrace() {

    $("#HTTPTraceTable").html('<tr>\n' +
        '                    <th>Timestamp</th>\n' +
        '                    <th>Method</th>\n' +
        '                    <th>URI</th>\n' +
        '                    <th>Response code</th>\n' +
        '                </tr>');

    var obj = httpobj["traces"];

    $("#all-httptrace").val(obj.length).text("All ("+obj.length+")");

    var numPerPage = $("#view-amt-http").val();


    for (var i = 0; i < numPerPage; i++) {

        if (i < obj.length) {
            var result = obj[i];

            var html = '<tr><td>'+result["timestamp"]+'</td><td>'+result["request"]["method"]+'</td><td>'+result["request"]["uri"]+'</td><td>'+result["response"]["status"]+'</td></tr>';

            $("#HTTPTraceTable").append(html);
        } else {
            return;
        }
    }

}

function renderEnv(data) {
    console.log(data["propertySources"][2]["properties"]);
    $("#env-os-name").text(data["propertySources"][2]["properties"]["os.name"]["value"] + ' ' + data["propertySources"][2]["properties"]["os.version"]["value"] + ' (' + data["propertySources"][2]["properties"]["os.arch"]["value"] + ')');
    $("#env-java-version").text(data["propertySources"][2]["properties"]["java.version"]["value"] + ' (' + data["propertySources"][2]["properties"]["java.version.date"]["value"] + ')');
    $("#env-java-runtime-version").text(data["propertySources"][2]["properties"]["java.runtime.version"]["value"]);
}