form split_channels
   sentence directory /directory
   integer channel
   sentence result /result
endform


Create Strings as file list... list 'directory$'/*.wav
numberOfFiles = Get number of strings
for ifile to numberOfFiles
   select Strings list
   fileName$ = Get string... ifile
   Read from file... 'directory$'/'fileName$'
	sound_name$ = selected$ ("Sound")

	select Sound 'sound_name$'
	Extract one channel... channel
	Save as WAV file: "'result$'/'fileName$'"
	
	
	select all
	minus Strings list
	Remove

endfor
