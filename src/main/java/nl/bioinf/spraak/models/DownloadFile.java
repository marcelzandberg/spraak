package nl.bioinf.spraak.models;

public class DownloadFile {

    private String contentType;
    private String dataBase64Encoded;
    private String filename;
    private String extension;


    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getDataBase64Encoded() {
        return dataBase64Encoded;
    }

    public void setDataBase64Encoded(String dataBase64Encoded) {
        this.dataBase64Encoded = dataBase64Encoded;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public DownloadFile(String contentType, String dataBase64Encoded, String filename, String extension) {
        this.contentType = contentType;
        this.dataBase64Encoded = dataBase64Encoded;
        this.filename = filename;
        this.extension = extension;
    }

    public DownloadFile(){

    }
}
