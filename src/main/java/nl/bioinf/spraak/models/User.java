package nl.bioinf.spraak.models;

/**
 *  created by Marcel Zandberg
 *  Modiefied during internship by Bas Kasemir
 */

public class User {

    private int id;
    private String first_name;
    private String last_name;
    private String email;
    private String userName;
    private String institute_abbr;
    private String full_name_institute;
    private String created_date;
    private String role;
    private String formattedRole;
    private String profile_picture;
    private String profile_language;
    private String password;
    private String totp_secret;
    private String passwordConfirm;
    private String institute;
    private int enabled;
    private String password_token;
    private String password_timestamp;
    private int password_reset_link_valid;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getRole() { return role; }

    public void setRole(String role) { this.role = role; }

    public String getProfile_picture() { return profile_picture; }

    public void setProfile_picture(String profile_picture) { this.profile_picture = profile_picture; }

    public String getProfile_language() { return profile_language; }

    public void setProfile_language(String profile_language) { this.profile_language = profile_language; }

    public String getFormattedRole() {
        return formatRole(role);
    }

    public String formatRole(String roletoformat) {
        String workingString = roletoformat.replaceAll("ROLE_", "").toLowerCase();
        return workingString.substring(0, 1).toUpperCase() + workingString.substring(1);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInstitute_abbr() {
        return institute_abbr;
    }

    public void setInstitute_abbr(String institute_abbr) {
        this.institute_abbr = institute_abbr;
    }

    public String getFull_name_institute() {
        return full_name_institute;
    }

    public void setFull_name_institute(String full_name_institute) {
        this.full_name_institute = full_name_institute;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }


    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getTotp_secret() {
        return totp_secret;
    }

    public void setTotp_secret(String totp_secret) {
        this.totp_secret = totp_secret;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getPassword_token() {
        return password_token;
    }

    public void setPassword_token(String password_token) {
        this.password_token = password_token;
    }

    public String getPassword_timestamp() {
        return password_timestamp;
    }

    public int getPassword_reset_link_valid() {
        return password_reset_link_valid;
    }

    public void setPassword_reset_link_valid(int password_reset_link_valid) {
        this.password_reset_link_valid = password_reset_link_valid;
    }

    public User(int id, String first_name, String last_name, String email, String userName, String institute_abbr, String full_name_institute, String created_date, String role, String profile_picture, String profile_language, String password, String totp_secret, String passwordConfirm, String institute, int enabled, String password_token, String password_timestamp, int password_reset_link_valid) {
        this.id = id;
        this.first_name = first_name;
        this.last_name =  last_name;
        this.email = email;
        this.userName = userName;
        this.institute_abbr = institute_abbr;
        this.full_name_institute = full_name_institute;
        this.created_date = created_date;
        this.role = role;
        this.profile_picture = profile_picture;
        this.profile_language = profile_language;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.institute = institute;
        this.enabled = enabled;
        this.password_token = password_token;
        this.password_timestamp = password_timestamp;
        this.totp_secret = totp_secret;
        this.password_reset_link_valid = password_reset_link_valid;
    }

    public User(){

    }


}
