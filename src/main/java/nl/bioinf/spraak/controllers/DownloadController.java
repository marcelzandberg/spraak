package nl.bioinf.spraak.controllers;
/**
 * Copyright 2020 Marcel Zandberg
 */

import com.google.gson.Gson;
import nl.bioinf.spraak.models.PlotData;
import nl.bioinf.spraak.models.User;
import nl.bioinf.spraak.service.LocaleService;
import nl.bioinf.spraak.service.UserService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
@RequestMapping("/download")
public class DownloadController {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;
    private final LocaleService localeService;

    @Value("${results-path}")
    private String results;

    @Value("${praat.files}")
    private String praatFiles;

    @Autowired
    public DownloadController(UserService userService, LocaleService localeService) {
        this.userService = userService;
        this.localeService = localeService;
    }

    public String downloadPath;

    @RequestMapping(path = "/get", produces = "application/json")
    public @ResponseBody
    String RequestFile(@RequestParam(name="randomHash") String randomHash,
                       @RequestParam(name="usePraat") String usePraat,
                       @RequestParam(name="useOpenSmile") String useOpenSmile,
                       @RequestParam(name = "plot") int whichPlot, HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth) throws IOException{
        logger.log(Level.INFO, "result data request");

        downloadPath = results+randomHash+"_"+principal.getName()+"\\";
        //path to dir
        String path = results+randomHash+"_"+principal.getName()+"\\";
        //model
        PlotData p = new PlotData();
        //file dir
        File praatResult = new File(path);
        //contains the file names
        String[] files = praatResult.list();
        //contains the file data
        List<String> data = new ArrayList<>();
        //the response
        String Response = "";

        Gson g = new Gson();

        //contains data to map to the plot model
        List<String> praatData = new ArrayList<>();
        List<String> openSmileData = new ArrayList<>();
        List<String> praatControlData = new ArrayList<>();
        List<String> openSmileControlData = new ArrayList<>();

        //has opensmile result as string
        String opensmileresult = "";
        //loops through the dir to get all opensmile files

        int fileN = 1;

        if(useOpenSmile.equals("1") && !usePraat.equals("1")){
            fileN = 0;
        }

        if(useOpenSmile.equals("1")){
            for(int i = 0; i < (files.length - fileN); i++){
                File file = new File(path+files[i]);
                //will get the result string of the opensmile file
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {

                    String lastLine = "";
                    String line;
                    while ((line = br.readLine()) != null) {
                        // process the line.
                        lastLine = line;
                    }
                    opensmileresult +=(lastLine + "\r\n");
                }
            }
            //adds the result string to the array
            openSmileData.add(opensmileresult);
        }


        if(usePraat.equals("1")){
            //praat file data
            File praatFile = new File(path+files[files.length -1]);
            praatData.add(FileUtils.readFileToString(praatFile, "UTF-8"));
        }

        if(whichPlot > 1){
            //parse controll data here
            //append the data to the list
            File controlData = new File("C:\\Users\\marcel\\Documents\\studie\\Stage\\spraak\\data\\controlData\\DATA_analyse_praatstudie_stripped_CSV.csv");
            try (BufferedReader br = new BufferedReader(new FileReader(controlData))) {
                String line;

                while ((line = br.readLine()) != null) {
                    // process the line.
                    if(line.startsWith("PP nummer (PRAAT)")) continue;
                    String[] elements = line.split(";");
                    //praat controll data
                    if(usePraat.equals("1")) {
                        String nsyll = elements[96];
                        String npause = elements[97];
                        String dur = elements[98];
                        String phonation = elements[99];
                        String speech = elements[100];
                        String art = elements[101];
                        String asd = elements[102];
                        praatControlData.add(nsyll + "," + npause + "," + dur + "," + phonation + "," + speech + "," + art + "," + asd);
                    }
                    //opensmile controll data
                    if(useOpenSmile.equals("1")){
                        String var1 = elements[84];
                        String var2 = elements[81];
                        String var3 = elements[87];
                        String var4 = elements[45];
                        String var5 = elements[23];
                        String var6 = elements[13];
                        String var7 = elements[88];
                        openSmileControlData.add(var1 + "," + var2 + "," + var3 + "," + var4 + "," + var5 + "," + var6 + "," + var7);
                    }
                }
            }
            p.setPraatControlData(praatControlData);
            p.setOpenSmileControlData(openSmileControlData);
        }

        //model to json obj
        p.setPraatResult(praatData);
        p.setOpenSmileResult(openSmileData);
        Response = g.toJson(p);
        logger.log(Level.INFO, "Returning jsonobj with result data");
        //returns array with the data
        return Response;
    }



    @GetMapping(path = "/selected")
    public void getSelected(@RequestParam(name = "sessions") String sessions ,String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response){
        logger.log(Level.INFO, "download selected zipfile request");
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);
        model.addAttribute("user", userData);


        // The path below is the root directory of data to be compressed
        File praatResult = new File(results+sessions+"\\");

        //TODO download selected folders in zip
        String[] files = praatResult.list();

        try {
            // Checks to see if the directory contains some files.
            if (files != null && files.length > (files.length -1)) {
                // Call the zipFiles method for creating a zip stream.
                byte[] zip = zipFiles(praatResult, files);
                logger.log(Level.INFO, "zipping folders to download");
                OutputStream out = response.getOutputStream();
                response.setContentType("application/zip");
                response.setHeader("Content-Disposition", "attachment; filename=Spraak_result.zip");

                out.write(zip);
                out.flush();
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        localeService.setLocale(principal, response, request);
        logger.log(Level.INFO,"downloading files");
    }




    /**
     * Sends the response back to the user / browser. The
     * content for zip file type is "application/zip".
     * */
    @GetMapping
    public void getFiles(String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.log(Level.INFO, "download zipfile request");
        username = principal.getName();
        User userData = userService.getUserByUserName(username, false);
        model.addAttribute("user", userData);

        // The path below is the root directory of data to be compressed
        File praatResult = new File(downloadPath);
        String[] files = praatResult.list();

        try {
            // Checks to see if the directory contains some files.
            if (files != null && files.length > (files.length -1)) {
                // Call the zipFiles method for creating a zip stream.
                byte[] zip = zipFiles(praatResult, files);
                logger.log(Level.INFO, "creating zip file to download");
                OutputStream out = response.getOutputStream();
                response.setContentType("application/zip");
                response.setHeader("Content-Disposition", "attachment; filename=Spraak_result.zip");

                out.write(zip);
                out.flush();
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        localeService.setLocale(principal, response, request);
        logger.log(Level.INFO,"downloading files");

    }

    

    /**
     * Compress the given directory with all its files.
     */
    private byte[] zipFiles(File directory, String[] files) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        byte bytes[] = new byte[2048];

        for (String fileName : files) {
            FileInputStream fis = new FileInputStream(directory.getPath() +
                    "\\" + fileName);
            BufferedInputStream bis = new BufferedInputStream(fis);

            zos.putNextEntry(new ZipEntry(fileName));

            int bytesRead;
            while ((bytesRead = bis.read(bytes)) != -1) {
                zos.write(bytes, 0, bytesRead);
            }
            zos.closeEntry();
            bis.close();
            fis.close();
        }
        zos.flush();
        baos.flush();
        zos.close();
        baos.close();
        logger.log(Level.INFO, "files ready for zipping");
        return baos.toByteArray();
    }

}
