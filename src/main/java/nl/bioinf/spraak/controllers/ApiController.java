package nl.bioinf.spraak.controllers;

/**
 * Copyright 2019 Bas Kasemir and Marcel Zandberg
 */

import com.google.gson.Gson;
import nl.bioinf.spraak.models.DownloadResult;
import nl.bioinf.spraak.models.DownloadFile;
import nl.bioinf.spraak.service.APIService;
import nl.bioinf.spraak.service.InstituteService;
import nl.bioinf.spraak.service.UserService;
import nl.bioinf.spraak.models.SoundFileData;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Api controller serves data via API.
 */
@RestController
@RequestMapping("/api")
public class ApiController {
    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;
    private final InstituteService instituteService;
    private final APIService apiService;


    @Value("${praat.files}")
    private String praatFiles;

    @Value("${results-path}")
    private String results;

    @Value("${path.slash}")
    private String pathslash;

    /**
     * Instantiates a new Api controller.
     *
     * @param userService      the user service
     * @param instituteService the institute service
     * @param apiService       the api service
     */
    @Autowired
    public ApiController(UserService userService, InstituteService instituteService, APIService apiService) {
        this.userService = userService;
        this.instituteService = instituteService;
        this.apiService = apiService;
    }

    /**
     * Request api string.
     *
     * @param url       the url
     * @param request   the request
     * @param response  the response
     * @param username  the username
     * @param principal the principal
     * @param auth      the auth
     * @return the string
     * @throws IOException the io exception
     */
    @RequestMapping(produces = "application/json")
    public String RequestAPI(@RequestParam(name="url") String url, HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth) throws IOException {
        logger.log(Level.INFO, "API controller got a request");
        String apiResponse;

        logger.log(Level.INFO, "API requestURL = " + url);

        // Init a new keyvaluepair list that contains the params sended to the API
        List<NameValuePair> arguments = new ArrayList<>(1);
        // Add the username to this list
        arguments.add(new BasicNameValuePair("username", principal.getName()));

        // Create an empty list of files to send
        List<MultipartFile> filesToSendList = new ArrayList<>();

        //makes a response for the api
        apiResponse = apiService.makeRequest(auth, principal, url, arguments, filesToSendList);
        return apiResponse;
    }

    /**
     * Request api string.
     *
     * @param url       the url
     * @param request   the request
     * @param response  the response
     * @param username  the username
     * @param principal the principal
     * @param auth      the auth
     * @return the string
     * @throws IOException the io exception
     */
    @PostMapping(path="/save", produces = "application/json")
    public String saveAPI(  @RequestParam(name="url") String url,
                            @RequestPart(value = "files[]", required = false) List<MultipartFile> filesList,
                            @RequestPart(value = "formData") String formData,
                            HttpServletRequest request,
                            HttpServletResponse response,
                            String username,
                            Principal principal,
                            Authentication auth) throws IOException {

        logger.log(Level.INFO, "API save got a request");
        String apiResponse;

        logger.log(Level.INFO, "API requestURL = " + url);

        // Init a new keyvaluepair list that contains the params sended to the API
        List<NameValuePair> arguments = new ArrayList<>(1);
        // Add the username to this list
        arguments.add(new BasicNameValuePair("username", principal.getName()));

        // Add the formdata to this list
        arguments.add(new BasicNameValuePair("formData", formData));

        apiResponse = apiService.makeRequest(auth, principal, url, arguments, filesList);

        //makes a response for the api
        return apiResponse;
    }

    /**
     * Request get api string.
     *
     * @param url       the url
     * @param request   the request
     * @param response  the response
     * @param username  the username
     * @param principal the principal
     * @param auth      the auth
     * @return the string
     * @throws IOException the io exception
     */
    @GetMapping(path="/get", produces = "application/json")
    public String RequestGetAPI(@RequestParam(name="url") String url, HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth) throws IOException {
        logger.log(Level.INFO, "API GET controller got a GET request");
        String apiResponse;

        logger.log(Level.INFO, "API GET requestURL = " + url);

        // Init a new keyvaluepair list that contains the params sended to the API
        List<NameValuePair> arguments = new ArrayList<>(1);
        // Add the username to this list
        arguments.add(new BasicNameValuePair("username", principal.getName()));

        //makes a response for the api
        apiResponse = apiService.makeGetRequest(auth, principal, url, arguments);

        // Returns the API response
        return apiResponse;

    }

    /**
     * Request download api.
     *
     * @param url           the url
     * @param request       the request
     * @param response      the response
     * @param username      the username
     * @param principal     the principal
     * @param auth          the auth
     * @param soundFileData the sound file data
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @RequestMapping(path = "/download")
    public void RequestDownloadAPI(@RequestParam(name="url") String url, HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth, SoundFileData soundFileData) throws IOException, SQLException {
        logger.log(Level.INFO, "API Download controller got a GET request");
        String apiResponse;

        logger.log(Level.INFO, "API download requestURL = " + url);

        // Init a new keyvaluepair list that contains the params sended to the API
        List<NameValuePair> arguments = new ArrayList<>(1);
        // Add the username to this list
        arguments.add(new BasicNameValuePair("username", principal.getName()));

        // Create an empty list of files to send
        List<MultipartFile> filesToSendList = new ArrayList<>();

        // Make a request, and set the response as a variable
        apiResponse = apiService.makeRequest(auth, principal, url, arguments, filesToSendList);

        Gson gson = new Gson();

        // Map the repsonse JSON to the corresponding model
        DownloadFile downloadFile = gson.fromJson(apiResponse, DownloadFile.class);

        // Encodes / decodes sounfile data so it can be downloaded
        byte[] encodedBytes = downloadFile.getDataBase64Encoded().getBytes(StandardCharsets.UTF_8);

        byte[] decodedBytes = Base64.getDecoder().decode(encodedBytes);

        String stringDecode = new String(decodedBytes, "UTF-8");

        // Convert the bytes to blob
        Blob blob = new SerialBlob(decodedBytes);

        // Set the MIME type in the response header
        response.setContentType(downloadFile.getContentType());
        // Set the response header data
        response.addHeader("Content-Disposition", "attachment; filename="+downloadFile.getFilename()+"."+downloadFile.getExtension());

        try {
            // get your file as InputStream
            InputStream is = blob.getBinaryStream();
            // copy it to response's OutputStream
            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }

    }

    /**
     * Request download api.
     *
     * @param url           the url
     * @param request       the request
     * @param response      the response
     * @param username      the username
     * @param principal     the principal
     * @param auth          the auth
     * @param soundFileData the sound file data
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @RequestMapping(path = "/download-internal")
    public boolean RequestInternalDownloadAPI(@RequestParam(name="url") String url,
                                              @RequestParam(name = "randomHash") String randomHash,
                                              HttpServletRequest request, HttpServletResponse response, String username, Principal principal, Authentication auth, SoundFileData soundFileData) throws IOException, SQLException {
        logger.log(Level.INFO, "API Download controller got a GET request");
        String apiResponse;

        logger.log(Level.INFO, "Session hash= "+randomHash);
        // Init a new keyvaluepair list that contains the params sended to the API
        List<NameValuePair> arguments = new ArrayList<>(1);
        // Add the username to this list
        arguments.add(new BasicNameValuePair("username", principal.getName()));

        //status for succes of download
        boolean status = true;

        // Create an empty list of files to send
        List<MultipartFile> filesToSendList = new ArrayList<>();

        // Make a request, and set the response as a variable
        apiResponse = apiService.makeRequest(auth, principal, url, arguments, filesToSendList);

        //check if the response contains something.
        //if empty then return the status that the download has failed
        if(apiResponse.isEmpty()){
            return false;
        }

        //json object
        Gson gson = new Gson();

        // Map the repsonse JSON to the corresponding model
        SoundFileData[] sfD = gson.fromJson(apiResponse, SoundFileData[].class);

        //create audio file dir
        File dir = new File(praatFiles + randomHash+pathslash);
        File dirL = new File(praatFiles + randomHash+pathslash+"channel_L");
        File dirR = new File(praatFiles + randomHash+pathslash+"channel_R");
        File dirE = new File(praatFiles + randomHash+pathslash+"extracted");

        makeDirectorys(dir, dirL, dirR, dirE);


        //loop over every file and write it to the session folder
        for(SoundFileData soundFileData1 : sfD){
            String channel;
            if(soundFileData1.getChannel().equals("L")){
                channel = "channel_L"+pathslash;
            }else if (soundFileData1.getChannel().equals("R")){
                channel = "channel_R"+pathslash;
            }else {
                channel = "";
            }

            try {
                File file = new File(praatFiles + randomHash+pathslash+channel+soundFileData1.getFilename());
                if (file.createNewFile()) {
                    byte[] encodedBytes = soundFileData1.getSoundFileDataBase64().getBytes();

                    byte[] decodedBytes = Base64.getDecoder().decode(encodedBytes);
                    try (FileOutputStream fos = new FileOutputStream(file)) {
                        fos.write(decodedBytes);
                        fos.flush();
                    }
                } else {
                    logger.log(Level.WARNING, "File already exists.");
                }
            }catch (IOException e){
                status = false;
                e.printStackTrace();
            }
        }

        return status;
    }

    private void makeDirectorys(File dir, File dirL, File dirR, File dirE) {
        dir.mkdir();
        dirL.mkdir();
        dirR.mkdir();
        dirE.mkdir();

        dir.setReadable(true);
        dir.setWritable(true);
        dirL.setReadable(true);
        dirL.setWritable(true);
        dirR.setReadable(true);
        dirR.setWritable(true);
        dirE.setReadable(true);
        dirE.setWritable(true);
    }


    @RequestMapping(path = "/results", produces = "application/json")
    public String getResults(String username, Model model, Principal principal, HttpServletRequest request, HttpServletResponse response){
        String json;
        //json object

        File result = new File(results);

        Gson gson = new Gson();

        List<DownloadResult> data = new ArrayList<>();

        int sessionId = 0;

        try {
            for (String session : result.list()) {
                DownloadResult d = new DownloadResult();
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
                //set sessionId
                d.setId(sessionId);

                //set session
                d.setSession(session);

                //find files in session
                File files = new File(results+session+"\\");
                d.setFileAmount(files.list().length);

                //set the creation date of the files
                d.setTimeStamp(dateFormat.format(files.lastModified()));


                d.setOwner(session.substring(session.lastIndexOf("_") +1));

                //add model to list
                data.add(d);
                //increment id with 1
                sessionId++;
            }
        }catch (NullPointerException e){
            e.getMessage();
        }

        json = gson.toJson(data);
        System.out.println(json);
        return json;
    }
}