package nl.bioinf.spraak.service;

/**
 *  created by Marcel Zandberg
 */

import nl.bioinf.spraak.data.DataSource;
import nl.bioinf.spraak.models.SpraakResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Open smile service.
 */
@Service
public class OpenSmileService{

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);


    private final DataSource dataSource;

    /**
     * Instantiates a new Open smile service.
     *
     * @param dataSource the data source
     */
    @Autowired
    public OpenSmileService(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /**
     * Insert open smile results int.
     *
     * @param principal       the principal
     * @param spraakResults   the spraak results
     * @param input           the input
     * @param usePartMatching the use part matching
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public int insertOpenSmileResults(Principal principal, SpraakResults spraakResults, String input , boolean usePartMatching) throws IOException, SQLException {
        if (!usePartMatching) {
            logger.log(Level.INFO, "praatservice insertpraatresults executed");
            return dataSource.insertOpenSmileResults(principal, spraakResults, input);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
}