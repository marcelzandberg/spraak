package nl.bioinf.spraak;


import nl.bioinf.spraak.storage.DeleteFiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.logging.Level;
import java.util.logging.Logger;


//@EnableAsync
@SpringBootApplication
@EnableScheduling
public class SpraakApplication {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);


    @Value("${results-path}")
    private String results;

    @Value("${praat.files}")
    private String praatFiles;


    private final DeleteFiles deleteFiles;


    @Autowired
    public SpraakApplication(DeleteFiles deleteFiles) {
        this.deleteFiles = deleteFiles;
    }

    public static void main(String[] args) {
            SpringApplication.run(SpraakApplication.class, args);
    }

    //delte all results and redundant temp audio files every day at 3:00 am
    @Scheduled(cron = "0 0 3 * * *")
    public void perform() throws Exception {
        logger.log(Level.INFO,"Executing scheduled task");
        boolean status;
        status = deleteFiles.cleanUpStorage();
        logger.log(Level.INFO, "Cleaned up the storage: "+status);
    }

}
