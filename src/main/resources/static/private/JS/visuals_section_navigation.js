// Copyright 2020 Marcel Zandberg

//total checked checkboxes
var num_of_checked_intro = 0;


//may|may not continue
$( document ).ready( function () {

    maxSectionNum = 0;
    mayGoFuther = true;

    $("#timesection").click(function () {

        var el = $("#plot-time-opensmile");


        if(isElementInViewport(el) === false){
            // Scroll to bottom
            document.querySelector('#plot-time-opensmile').scrollIntoView({
                behavior: 'smooth'
            });
            $("#scroll-time").removeClass("fa-chevron-down");
            $("#scroll-time").addClass("fa-chevron-up");
        }else{
            //scroll to top
            document.querySelector('#plot').scrollIntoView({
                behavior: 'smooth'
            });

            $("#scroll-time").removeClass("fa-chevron-up");
            $("#scroll-time").addClass("fa-chevron-down");
        }

    });


    $("#boxsection").click(function () {
        var el = $("#plot-box-openSmile");


        if(isElementInViewport(el) === false){
            // Scroll to bottom
            document.querySelector('#plot-box-openSmile').scrollIntoView({
                behavior: 'smooth'
            });
            $("#scroll-box").removeClass("fa-chevron-down");
            $("#scroll-box").addClass("fa-chevron-up");
        }else{
            //scroll to top
            document.querySelector('#plot').scrollIntoView({
                behavior: 'smooth'
            });

            $("#scroll-box").removeClass("fa-chevron-up");
            $("#scroll-box").addClass("fa-chevron-down");
        }
    });

    function isElementInViewport (el) {

        // Special bonus for those using jQuery
        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /* or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
        );
    }

});





//ckecks if checkbox select all is clicked in pat table
//uppon click show or hide de continue btn
$(document).on('click', '.select-all', function () {

    if ($(this).prop('checked')) {
        mayGoFuther = true;
        $(".intro-btn").removeClass("hideonload-important");
        $(".intro-note").removeClass("hideonload-important");
    } else {
        mayGoFuther = false;
        $(".intro-btn").addClass("hideonload-important");
        $(".intro-note").addClass("hideonload-important");
    }

});

//ckecks if checkbox is clicked in pat table
$(document).on('click', '.result-checkbox-intro', function() {

    if (this.checked) {
        num_of_checked_intro ++;
        maxSectionNum = 1;
        checkForNumOfIntroChecked();
    } else {
        num_of_checked_intro --;
        maxSectionNum = 0;
        checkForNumOfIntroChecked();
    }

});



$(document).on('click', '.neededid', function () {

    var resid = '#check' + this.id;


    if(!$(resid).prop('checked')){
        $(resid).prop("checked", true);
        num_of_checked_intro ++;
        maxSectionNum = 1;
        checkForNumOfIntroChecked();
    }else{
        $(resid).prop("checked", false);
        num_of_checked_intro --;
        maxSectionNum = 0;
        checkForNumOfIntroChecked();
    }

});


//shows continue btn when 2 or more checkboxes are checked
function checkForNumOfIntroChecked() {

    if (num_of_checked_intro > 1) {
        mayGoFuther = true;
        $(".intro-btn").removeClass("hideonload-important");
        $(".intro-note").removeClass("hideonload-important");
    } else {
        mayGoFuther = false;
        $(".intro-btn").addClass("hideonload-important");
        $(".intro-note").addClass("hideonload-important");
    }
}








