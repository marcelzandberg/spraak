package nl.bioinf.spraak.models;

/**
 *  created by Marcel Zandberg
 */

import java.io.File;

public class SpraakResults {

    private int id;
    private String patientNumber;
    private String soundFile;
    private File soundFileData;
    private String userName;
    private int nsyll;
    private int npause;
    private double duration;
    private double phonationTime;
    private double speechRate;
    private double articulation;
    private double ASD;
    private String date;
    private String spectogram;
    private String spectogram1;
    private String spectogram2;
    private String spectogram3;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSoundFile() {
        return soundFile;
    }

    public void setSoundFile(String soundFile) {
        this.soundFile = soundFile;
    }

    public int getNsyll() {
        return nsyll;
    }

    public void setNsyll(int nsyll) {
        this.nsyll = nsyll;
    }

    public int getNpause() {
        return npause;
    }

    public void setNpause(int npause) {
        this.npause = npause;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getPhonationTime() {
        return phonationTime;
    }

    public void setPhonationTime(double phonationTime) {
        this.phonationTime = phonationTime;
    }

    public double getSpeechRate() {
        return speechRate;
    }

    public void setSpeechRate(double speechRate) {
        this.speechRate = speechRate;
    }

    public double getArticulation() {
        return articulation;
    }

    public void setArticulation(double articulation) {
        this.articulation = articulation;
    }

    public double getASD() {
        return ASD;
    }

    public void setASD(double ASD) {
        this.ASD = ASD;
    }

    public String getPatientNumber() {
        return patientNumber;
    }

    public void setPatientNumber(String patientNumber) {
        this.patientNumber = patientNumber;
    }

    public File getSoundFileData() {
        return soundFileData;
    }

    public void setSoundFileData(File soundFileData) {
        this.soundFileData = soundFileData;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSpectogram() {
        return spectogram;
    }

    public void setSpectogram(String spectogram) {
        this.spectogram = spectogram;
    }

    public String getSpectogram1() {
        return spectogram1;
    }

    public void setSpectogram1(String spectogram1) {
        this.spectogram1 = spectogram1;
    }

    public String getSpectogram2() {
        return spectogram2;
    }

    public void setSpectogram2(String spectogram2) {
        this.spectogram2 = spectogram2;
    }

    public String getSpectogram3() {
        return spectogram3;
    }

    public void setSpectogram3(String spectogram3) {
        this.spectogram3 = spectogram3;
    }

    public SpraakResults(int id, String patientNumber, String soundFile, String userName, int nsyll, int npause, double duration, double phonationTime, double speechRate, double articulation, double ASD, File soundFileData, String date, String spectogram, String spectogram1, String spectogram2, String spectogram3) {
        this.id = id;
        this.patientNumber = patientNumber;
        this.soundFile = soundFile;
        this.userName = userName;
        this.nsyll = nsyll;
        this.npause = npause;
        this.duration = duration;
        this.phonationTime = phonationTime;
        this.speechRate = speechRate;
        this.articulation = articulation;
        this.ASD = ASD;
        this.soundFileData = soundFileData;
        this.date = date;
        this.spectogram = spectogram;
        this.spectogram1 = spectogram1;
        this.spectogram2 = spectogram2;
        this.spectogram3 = spectogram3;
    }

    public SpraakResults(){

    }

}
