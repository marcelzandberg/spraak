package nl.bioinf.spraak.data;

/**
 *  created by Marcel Zandberg
 */

import nl.bioinf.spraak.models.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

/**
 * The interface Data source.
 */
public interface DataSource {

    /**
     * Gets institute by abbreviation.
     *
     * @param abbreviation the abbreviation
     * @return the institute by abbreviation
     */
    Institute getInstituteByAbbreviation(String abbreviation);

    /**
     * Gets institutes.
     *
     * @return the institutes
     */
    List<Institute> getInstitutes();

    /**
     * Gets user by user name.
     *
     * @param userName the user name
     * @return the user by user name
     */
    User getUserByUserName(String userName);

    /**
     * Gets user data that is necessary for login by username.
     *
     * @param username the user name
     * @return the user by user name
     */
    User getUserDataForLogin(String username);

    /**
     * Gets user by password.
     *
     * @param password the password
     * @return the user by password
     */
    User getUserByPassword(String password);

    /**
     * Gets all users.
     *
     * @param namePattern the name pattern
     * @return the all users
     */
    List<User> getAllUsers(String namePattern);

    /**
     * Gets user by email.
     *
     * @param email the email
     * @return the user by email
     */
    User getUserByEmail(String email);

    /**
     * Insert institute int.
     *
     * @param institute the institute
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertInstitute(Institute institute) throws IOException, SQLException;;

    /**
     * Insert user int.
     *
     * @param registerUser the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertUser(UserNew registerUser) throws IOException, SQLException;

    /**
     * Delete from usertable int.
     *
     * @param username the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int deleteUserFromUserTable(String username) throws IOException, SQLException;

    /**
     * Delete from role table.
     *
     * @param username the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int deleteUserFromRoleTable(String username) throws IOException, SQLException;

    /**
     * Delete from Login attempts table.
     *
     * @param username the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int deleteUserFromLATable(String username) throws IOException, SQLException;

    /**
     * Insert sound file int.
     *
     * @param principal     the principal
     * @param spraakResults the spraak results
     * @param file          the file
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertSoundFile(Principal principal, SpraakResults spraakResults, MultipartFile file) throws IOException, SQLException;

    /**
     * Insert praat results int.
     *
     * @param spraakResults the spraak results
     * @param principal     the principal
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertPraatResults(SpraakResults spraakResults, Principal principal) throws IOException, SQLException;

    /**
     * Insert open smile results int.
     *
     * @param principal     the principal
     * @param spraakResults the spraak results
     * @param input         the input
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertOpenSmileResults(Principal principal, SpraakResults spraakResults, String input) throws IOException, SQLException;

    /**
     * Insert praat max upload int.
     *
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertPraatMaxUpload() throws IOException, SQLException;

    /**
     * Update user int.
     *
     * @param registerUser the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updateUser(UserNew registerUser) throws IOException, SQLException;

    /**
     * Insert role int.
     *
     * @param registerUser the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int insertRole(UserNew registerUser) throws IOException, SQLException;

    /**
     * Update role int.
     *
     * @param registerUser the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updateRole(UserNew registerUser) throws IOException, SQLException;

    /**
     * Update role int.
     *
     * @param user the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updateAccountActivation(User user, boolean activated) throws IOException, SQLException;

    /**
     * Update password link valid
     *
     * @param username the username
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updatePasswordLinkValid(String username, boolean bool) throws IOException, SQLException;

    /**
     * Update password reset token
     *
     * @param username the username
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updatePasswordReset(String username, String datetime) throws IOException, SQLException;

    /**
     * Update password token
     *
     * @param user the user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updatePasswordToken(User user, String token) throws IOException, SQLException;

    /**
     * Update password token
     *
     * @param user the user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updatePassword(User user) throws IOException, SQLException;

    /**
     * Update loginattempts int.
     *
     * @param loginAttempts the register user
     * @return the int
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    int updateLoginAttempts(LoginAttempts loginAttempts) throws IOException, SQLException;


    LoginAttempts getLoginAttemptsByUsername(String userName);

    int addLoginAttempts(UserNew registerUser);

    int getNumberOfUsernames(String userName);
}
