package nl.bioinf.spraak.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EmailService {

    @Value(value = "${mail.smtp_auth}")
    private String smtpauth;

    @Value(value = "${mail.smtp_starttls_enable}")
    private String starttls;

    @Value(value = "${mail.smtp_host}")
    private String host;

    @Value(value = "${mail.smtp_port}")
    private String port;

    @Value("${mail.auth_account}")
    private String account;

    @Value("${mail.auth_password}")
    private String password;

    @Value("${mail.from_address}")
    private String address;

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    public String sendmail(String subject, String content, String ContentType, String recipient) throws AddressException, MessagingException, IOException {

        logger.log(Level.INFO, "About to send an email");

        Properties props = new Properties();
        props.put("mail.smtp.auth", smtpauth);
        props.put("mail.smtp.starttls.enable", starttls);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(account, password);
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(address, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        msg.setSubject(subject);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent("Some text", "text/html");

        /*Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        MimeBodyPart attachPart = new MimeBodyPart();

        attachPart.attachFile("/var/tmp/image19.png");
        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);*/
        Transport.send(msg);

        logger.log(Level.INFO, "Email sent");

        return "sent";
    }

    public EmailService() {

    }

}
