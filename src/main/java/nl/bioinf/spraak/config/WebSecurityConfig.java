package nl.bioinf.spraak.config;

/**
 *  created by Bas Kasemir
 */

import nl.bioinf.spraak.auth.CustomAuthProvider;
import nl.bioinf.spraak.auth.CustomWebAuthenticationDetailsSource;
import nl.bioinf.spraak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

/**
 * Web security config.
 */
@Configuration
/*@ComponentScan(basePackages = { "nl.bioinf.spraak" })*/
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * The Data source.
     */
    @Autowired
    DataSource dataSource;

    @Autowired
    UserService userService;

    @Autowired
    private CustomAuthProvider authProvider;

    @Autowired
    private CustomWebAuthenticationDetailsSource authenticationDetailsSource;
    /**
     * Config authentication.
     *
     * @param auth the auth
     * @throws Exception the exception
     */
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        try {
            auth.authenticationProvider(authProvider);
        } catch (BadCredentialsException ex) {
            System.out.println("ex = " + ex);
        }
    }

    /**
     * Password encoder encodes password.
     *
     * @return the password encoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                    // Paths that don't need any authentication
                    .antMatchers("/public/**", "/login/**", "/user/activate", "/user/activate/**", "/user/password", "/user/password/**").permitAll()
                    .antMatchers("/private/admin/**", "/user/create", "/user/modify, /settings, /health").access("hasRole('ROLE_ADMIN')")
                .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .authenticationDetailsSource(authenticationDetailsSource)
                    .loginPage("/login")
                    .permitAll()
                    .and()
                .logout()
                    .permitAll()
                    .and()
                .csrf();
    }

    public AuthenticationProvider authProvider() {
        CustomAuthProvider authProvider = new CustomAuthProvider(userService);
        return authProvider;
    }
}