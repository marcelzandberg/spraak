package nl.bioinf.spraak.applications;

/**
 *  created by Marcel Zandberg
 */

import nl.bioinf.spraak.service.APIService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;


import java.io.File;
import java.io.IOException;

import java.security.Principal;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Open smile runner.
 */
@Component
public class OpenSmileRunner {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();

    Logger logger = Logger.getLogger(className);

    @Value("${openSmile.executable-path}")
    private String openSmileExecutable;


    @Value("${praat.files}")
    private String praatFiles;

    @Value("${results-path}")
    private String results;

    @Value("${path.slash}")
    private String pathslash;

    @Value("${openSmile.executable-path}")
    private String openSmileExecutablePath;


    public boolean openSmileStatus;

    /**
     * The Api service.
     */
    @Autowired
    APIService apiService;



    /**
     * Command line runner.
     *
     *
     * @param principal     the principal
     */
    public void commandLineRunner(Principal principal, Authentication auth, String randomHash, String outDir) throws IOException {
        //audio file dir
        File list = new File(outDir+pathslash);

        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        try{

            for(File file : list.listFiles()){
                pr = rt.exec(openSmileExecutablePath+ " -C "+openSmileExecutable+"config"+pathslash+"eGeMAPSv01a.conf -I "+outDir+pathslash+file.getName()+" -O "+results+randomHash+"_"+principal.getName()+pathslash+file.getName().replaceFirst("[.][^.]+$", ".arff"));
                aliveStatus(pr.isAlive());
            }


            logger.log(Level.INFO,"OpenSmile is running");
        }catch (Exception e){
            System.out.println(e.toString());
            logger.log(Level.WARNING, "could not parse the lines");
            e.printStackTrace();
            throw new IOException();
        }

        while (pr.isAlive()){
            aliveStatus(pr.isAlive());
        }
        aliveStatus(pr.isAlive());
    }

    public void aliveStatus(boolean status){
        openSmileStatus = status;
    }

}
