package nl.bioinf.spraak.auth;

/**
 * Copyright 2019 Bas Kasemir
 */
public class SecurityConstants {
    /**
     * The constant TOKEN_PREFIX.
     */
    // Sets the prefix for the token
    public static final String TOKEN_PREFIX = "Bearer ";
    // Sets the key for the header
    public static final String HEADER_STRING = "Authorization";
}