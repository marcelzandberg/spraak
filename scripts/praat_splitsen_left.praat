form praat_splitsen_left
   sentence directory /directory
   sentence result /result
endform



list = Create Strings as file list... list 'directory$'/*.wav
n = Get number of strings

for i to n
	#create all the variables from the filename in the list.
	selectObject: list
	filename$ = Get string: i
	basename$ = filename$ - ".wav"
	textgrid1$ = "TextGrid " + basename$ + "_ch1"

	textgrid1$ = replace$ (textgrid1$, ".", "_", 3)
	ch1name$ = "Sound " + basename$ + "_ch1" 
	ch2name$ = "Sound " + basename$ + "_ch2"
	ch1name$ = replace$ (ch1name$, ".", "_", 3)
	ch2name$ = replace$ (ch2name$, ".", "_", 3)

	#load in the file as a sound, extract 2 channels.
	sound = Read from file: directory$ + filename$
	Extract all channels
	
	selectObject: ch1name$
	To TextGrid (silences): 100, 0, -25, 1, 0.1, "interviewer_silent", "interviewer_speaks"
	selectObject: textgrid1$, ch2name$ 
	Extract intervals where: 1, "no", "is equal to", "interviewer_silent"
	
	#count and save all the sound intervals for later deletion.

	total_parts = numberOfSelected()
	for x to total_parts
  		part[x] = selected(x)
	endfor
	
	Concatenate recoverably
	selectObject: "Sound chain"
	nowarn Save as WAV file: result$ + filename$ + ".wav"
	
	#remove all the sound segments from earlier
	for x to total_parts
  		plusObject: part[x]
	endfor
	Remove

	#clean up some other variables left over.
	removeObject: "TextGrid chain", sound, ch1name$, ch2name$, textgrid1$


endfor
