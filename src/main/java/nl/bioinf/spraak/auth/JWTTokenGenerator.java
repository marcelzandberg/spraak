package nl.bioinf.spraak.auth;

/**
 * Copyright 2019 Bas Kasemir
 */

import com.auth0.jwt.JWT;
import nl.bioinf.spraak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import nl.bioinf.spraak.models.User;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

/**
 * Jwt token generator.
 */
@Component
public class JWTTokenGenerator {

    // Sets the secret key for encrypting the JWT. NOTE: This must be the same as in each API
    @Value("${auth.secret}")
    private  String secret;

    // Sets the expiration time (in ms)
    @Value("${auth.expiration_time}")
    private String exp_time_string;

    @Autowired
    UserService userService;

    /**
     * Generate token string.
     *
     * @param auth the auth
     * @return the string
     */
    public String generateToken(Authentication auth, User user, String exptime, String jwtSecret) {

        System.out.println("exptime = " + exptime);
        // Check if a specific expire time has been provided. If not, set the application default
        if (exptime == null) {
            exptime = exp_time_string;
        }

        // Check if a specific expire time has been provided, If not, set the application default
        if (jwtSecret == null) {
            jwtSecret = secret;
        }
        long expiration_time = Long.parseLong(exptime);

        if (user == null && auth  != null) {
            user = userService.getUserByUserName(auth.getPrincipal().toString(), false);
        }

        String token = JWT.create()
                .withSubject(user.getUserName())
                .withExpiresAt(new Date(System.currentTimeMillis() + expiration_time))
                .sign(HMAC512(jwtSecret.getBytes()));

        return token;

    }

}
