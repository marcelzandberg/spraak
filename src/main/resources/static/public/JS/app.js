/**
 * Copyright 2019 Bas Kasemir and Marcel Zandberg
 */

// Set the version number
var app_name = "Spraak";

// Set the version number
var app_version = "1.0.0";

// colors

var spraak_colors = {
    "royal_blue" : "rgba(6, 82, 221, 1.0)",
    "royal_blue_li_shade" : "rgba(6, 153, 244, 1.0)",
    "thundercloud_grey" : "rgba(127, 143, 166,1.0)",
    "sea_blue" : "rgba(45, 152, 218,1.0)",
    "construction_yellow" : "rgba(254, 211, 48,1.0)",
    "dogwood_blossom_red" : "rgba(235, 59, 90,1.0)",
    "download_green" : "rgba(76, 209, 55,1.0)",

};

$(document).ready( function () {
    //Write something to the console
    console.log(app_name+" "+app_version);

    // Localechanger
    $("span[data-changelang]").click(function () {
        var lang = $(this).attr("data-changelang");
        window.location.replace('?lang=' + lang);
    });

    // Display the version in the about popup, or where else needed.
    $(".PrintVersion").text(app_version);

    // Add a "this link is external" icon after each external link by checking if the hosts are the same
    // It won't be added to images, to keep the design clean
    $( "a" ).each(function() {
        if (this.host !== window.location.host) {
            if($(this).find("img").length){
                return
            }
            $(this).after("<span class=\"no-underline\"> <i class=\"fas fa-external-link-alt no-underline\"></i></span>");
        }
    });

    // Check if the web app is ran as standsole (progressive web app) on an iOS device
    if (("standalone" in window.navigator) && window.navigator.standalone) {
        // The web app is in PWA mode. Now we will add some classes to fill up the statusbar and add some extra padding to the content
        $(".header").addClass("header-pwa");
        $("body").addClass("body-pwa");

        // Event handler for when the device is being rotated. The statusbar is hidden in landscape mode, so the extra padding isn't needed anymore.
        $( window ).on( "orientationchange", function( event ) {

            // Because of the transition animations in iOS, the viewport width and height change after a few seconds.
            // Therefore is a timeout of 50ms set before the viewport sizes are requested.
            setTimeout( function () {
                // Check if in landscape mode. If so, add the extra padding classes, else remove these classes
                if (window.innerWidth < window.innerHeight) {
                    $(".header > nav").removeClass("nav-pwa");
                    $(".header > nav").addClass("nav-pwa");
                    $(".header").removeClass("header-pwa");
                    $(".header").addClass("header-pwa");
                } else {
                    $(".header > nav").removeClass("nav-pwa");
                    $(".header").removeClass("header-pwa");
                }
            }, 50);

        });

    }

    $('.radio-button').click(function() {
        if($(this).children('input[type="radio"]').is(':checked')) {
            $(this).parent(".radio-container").children(".radio-button").removeClass("selected");
            $(this).addClass("selected");
        }
    });

    // Add a caret to each select field
    $("select").each(function () {
        $(this).after('<i class="fas fa-caret-down selctor-arw-filter"></i>');
    });

    // Add visual form validation to the required input fields (see readme on how to apply this function to a inputfield).
    $('input[data-req="true"]').keyup( function () {
        // Get the minimum required length
        var minlen = 0;

        let hasDatareqmin = $(this).attr('data-req_min');
        let hasMinlength = $(this).attr("minlength");

        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if (typeof hasDatareqmin !== typeof undefined && hasDatareqmin !== false) {
            minlen = hasDatareqmin;
        } else if (typeof hasMinlength !== typeof undefined && hasMinlength !== false) {
            minlen = hasMinlength;
        } else {
            console.warn("Using default 0 since no minimum length is set for field with name "+$(this).attr("name")+' id '+$(this).attr("id"))
        }
        // Check if the current length is sufficient. If so, remove the invalid border class; if not, add the invalid border class
        if ($(this).val().length < minlen ) {
            $(this).addClass("invalid-form");
            return false
        } else {
            $(this).removeClass("invalid-form");
        }
    });


    // inserts the values of the selected user in the table of registration page
    $('.modify').change(function(){
        var userobj = document.getElementById("selected-user").value;
        var elements = userobj.split(",");
        console.log(elements);
        console.log(elements[3]);
        document.getElementById('userFirstName').value = elements[0];
        document.getElementById('userLastName').value = elements[1];
        document.getElementById('userEmail').value = elements[2];
        document.getElementById('userName').value = elements[3];
        document.getElementById('userInstitute').value = elements[5];
        document.getElementById('userProfileLanguage').value = elements[4];
    });


    $('.submit-user-form').click( function() {
        $("#register").submit();
    });

    $('.submit-institute-form').click( function() {
        $("#institute").submit();
    });

    $('.reset-user-form').click( function() {
        $("#register").trigger("reset");
    });

    $('.pick-user-language').click( function() {
        $("#language").select();
    });

    // ********
    // SWITCHES
    // ********

    // Event handler for a click on a switch (div with class "checkbox")
    $('.checkbox').click( function() {

        // Check if the item may be clicked
        if ($(this).hasClass("not-allowed")) {
            return
        }

        //toggle the class checked for this div
        $(this).toggleClass("checked");

        // construct a selector
        var selector = "#input-"+$(this).attr('id');

        // check if the checkbox is already checked or not and change the values
        if ($(selector).val() == 1) {
            $(selector).val("0");
            //$(".checkbox").removeClass("not-allowed").attr("title","");
        } else {
            $(selector).val("1");
            //$(".checkbox").addClass("not-allowed").attr("title","To select this option, please disable the selected option first");
            //$(".checked").removeClass("not-allowed").attr("title","");
        }

    });

    
});

// Construct and format the current date. Set as global variable so it can be used in other JS files
var d = new Date,
day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate(),
month = ((d.getMonth()+1) < 10 ? "0" + (d.getMonth()+1) : (d.getMonth()+1)),
year = d.getFullYear();