package nl.bioinf.spraak.service;

import nl.bioinf.spraak.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Locale service sets the locale.
 */
@Service
public class LocaleService {

    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    private final UserService userService;

    /**
     * Instantiates a new Locale service.
     *
     * @param userService the user service
     */
    @Autowired
    public LocaleService(UserService userService) {
        this.userService = userService;
    }


    /**
     * Sets locale.
     *
     * @param principal the principal
     * @param response  the response
     * @param request   the request
     * @return the locale
     */
    public SessionLocaleResolver setLocale(Principal principal, HttpServletResponse response, HttpServletRequest request) {


        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();

        if (principal != null) {
            String username = principal.getName();
            User userData = userService.getUserByUserName(username, false);

            String prefLocale = userData.getProfile_language().toLowerCase();
            String prefLocaleLang = prefLocale.split("-")[0];
            String prefLocaleCountry = prefLocale.split("-")[1].toUpperCase();
            Locale prefnewLoc = new Locale(prefLocaleLang, prefLocaleCountry);
            sessionLocaleResolver.setLocale(request, response, prefnewLoc);
        } else {
            sessionLocaleResolver.setLocale(request, response, request.getLocale());
        }

        return sessionLocaleResolver;

    }

}
