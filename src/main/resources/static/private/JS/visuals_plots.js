// Copyright 2020 Marcel Zandberg

var praatData = [];
var openSmileData = [];


var praatControlData = [];
var openSmileControlData = [];

//labels for time entrys
var labels = ["First entry", "Second entry", "Third entry", "Fourth entry"];


/**************************
 * FETCH RESULTS FUNCTION *
 **************************/

//read the data
function getData(data) {
    console.log(data);
    //has the data in variables
    var praatResults;
    var openSmileResults;

    var praatControlls;
    var openSmileControlls;

    //checks if there is even any data
    if(data["praatResult"] === undefined){
        praatResults = [];
    }else {
        praatResults = data["praatResult"];
    }

    if(data["openSmileResult"] === undefined){
        openSmileResults = [];
    }else {
        openSmileResults = data["openSmileResult"];
    }

    if(data["praatControlData"] === undefined){
        praatControlls = [];
    }else {
        praatControlls = data["praatControlData"];
    }

    if(data["openSmileControlData"] === undefined){
        openSmileControlls = [];
    }else {
        openSmileControlls = data["openSmileControlData"];
    }



    //if praat is used
    if(praatResults.length > 0){
        var getPraatResult = praatResults[praatResults.length -1];
        var praatResult = getPraatResult.split("\r\n");
        var praatResultToInt = [];

        var praatControllToInt = [];

        //praat result collumns
        var nsyll = [];
        var npause = [];
        var dur = [];
        var phonationtime = [];
        var speechrate = [];
        var articulationrate = [];
        var ASD = [];

        //praat controll collumns
        var nsyllControll = [];
        var npauseControll = [];
        var durControll = [];
        var phonationtimeControll = [];
        var speechrateControll = [];
        var articulationrateControll = [];
        var ASDControll = [];

        //console.log(praatControlls);
        //converts all lines of praatcontroll data to integer
        for (i = 0; i < (praatControlls.length); i++){
            praatControllToInt.push(praatControlls[i].split(",").map(Number));
        }


        //converts all lines of praat data to integer
        for (i = 0; i < (praatResult.length -1); i++){
            praatResultToInt.push(praatResult[i].split(",").map(Number).slice(1));
        }

        //map the result to the right praat variable
        //if(patnr.length > 1) {
        for (i = 0; i < praatResultToInt.length; i++){
            nsyll.push(praatResultToInt[i][0]);
            npause.push(praatResultToInt[i][1]);
            dur.push(praatResultToInt[i][2]);
            phonationtime.push(praatResultToInt[i][3]);
            speechrate.push(praatResultToInt[i][4]);
            articulationrate.push(praatResultToInt[i][5]);
            ASD.push(praatResultToInt[i][6]);
        }
        //praat data for boxplot
        praatData.push(nsyll, npause, dur, phonationtime, speechrate, articulationrate, ASD);

        for (i = 0; i < praatControllToInt.length; i++){
            nsyllControll.push(praatControllToInt[i][0]);
            npauseControll.push(praatControllToInt[i][1]);
            durControll.push(praatControllToInt[i][2]);
            phonationtimeControll.push(praatControllToInt[i][3]);
            speechrateControll.push(praatControllToInt[i][4]);
            articulationrateControll.push(praatControllToInt[i][5]);
            ASDControll.push(praatControllToInt[i][6]);
        }
        praatControlData.push(nsyllControll, npauseControll, durControll, phonationtimeControll, speechrateControll, articulationrateControll, ASDControll);
    }



    //if opensmile is used
    if(openSmileResults.length > 0){
        var getOpenSmileResult = openSmileResults[openSmileResults.length -1];
        var openSmileResult = getOpenSmileResult.split("\r\n");
        var openSmileResultToInt = [];

        var openSmileControllToInt = [];

        //opensmile result collumns
        var var1 = [];
        var var2 = [];
        var var3 = [];
        var var4 = [];
        var var5 = [];
        var var6 = [];
        var var7 = [];

        //opensmile Controll collumns
        var var1Controll = [];
        var var2Controll = [];
        var var3Controll = [];
        var var4Controll = [];
        var var5Controll = [];
        var var6Controll = [];
        var var7Controll = [];

        //converts all lines of opensmile data to integer
        for (i = 0; i < (openSmileResult.length -1); i++){
            openSmileResultToInt.push(openSmileResult[i].split(",").map(Number).slice(1));
        }

        //converts all lines of opensmile data to integer
        for (i = 0; i < (openSmileControlls.length -1); i++){
            openSmileControllToInt.push(openSmileControlls[i].split(",").map(Number));
        }

        //map the result to the right opensmile variable
        for (i = 0; i < openSmileResultToInt.length; i++){
            var1.push(openSmileResultToInt[i][82]);
            var2.push(openSmileResultToInt[i][79]);
            var3.push(openSmileResultToInt[i][85]);
            var4.push(openSmileResultToInt[i][49]);
            var5.push(openSmileResultToInt[i][21]);
            var6.push(openSmileResultToInt[i][11]);
            var7.push(openSmileResultToInt[i][86]);
        }
        //opensmile data for boxplot
        openSmileData.push(var1, var2, var3, var4, var5, var6, var7);

        //map the result to the right opensmile variable
        for (i = 0; i < openSmileControllToInt.length; i++){
            var1Controll.push(openSmileControllToInt[i][0]);
            var2Controll.push(openSmileControllToInt[i][1]);
            var3Controll.push(openSmileControllToInt[i][2]);
            var4Controll.push(openSmileControllToInt[i][3]);
            var5Controll.push(openSmileControllToInt[i][4]);
            var6Controll.push(openSmileControllToInt[i][5]);
            var7Controll.push(openSmileControllToInt[i][6]);
        }
        //opensmile data for boxplot
        openSmileControlData.push(var1Controll, var2Controll, var3Controll, var4Controll, var5Controll, var6Controll, var7Controll);
    }

    console.log(openSmileData);
    console.log(openSmileControlData);

    //amount of lables to be set equal to amount of entrys
    var names = [];
    for(i =0; i < patids.length; i++){
        names.push(labels[i])
    }

    /***********************
     * PLOT CONFIGURATIONS *
     ***********************/
    var barOptionsPraat = {
        responsive: true,
        legend: {
            position: 'top',
            labels: {
                boxWidth: 0
            }
        },
        title:{
            display: false

        },
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        },
        animation: {
            duration: 2000
        }
    };

    var barOptionsOpenSmile = {
        responsive: true,
        legend: {
            position: 'top',
            labels: {
                boxWidth: 0
            }
        },
        title:{
            display: false
        },
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        },
        animation: {
            duration: 2000
        }
    };

    var boxOptionsPraat = {
        responsive: true,
        legend: {
            position: 'top'
        },
        title: {
            display: true,
            text: 'Praat data patient vs Control'
        }
    };

    var boxOptionsOpenSmile = {
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'openSmile data patient vs Control'
        }
    };

    //colors
    window.chartColors = {
        red: 'rgba(255, 99, 132, 0.2)',
        orange: 'rgb(255, 159, 64, 0.2)',
        yellow: 'rgba(255, 206, 86, 0.2)',
        green: 'rgba(75, 192, 192, 0.2)',
        blue: 'rgba(54, 162, 235, 0.2)',
        purple: 'rgb(153, 102, 255, 0.2)',

        redback: 'rgba(255, 99, 132, 1)',
        orangeback: 'rgb(255, 159, 64, 1)',
        yellowback: 'rgba(255, 206, 86, 1)',
        greenback: 'rgba(75, 192, 192, 1)',
        blueback: 'rgba(54, 162, 235, 1)',
        purpleback: 'rgb(153, 102, 255, 1)'
    };

    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    var color = [ window.chartColors.red,
        window.chartColors.blue,
        window.chartColors.yellow,
        window.chartColors.green,
        window.chartColors.purple];

    var randomColor = shuffle(color);

    /************************
     * PLOT-TIME DATA PRAAT *
     ************************/
    var nsyllData = {
        labels: names,
        datasets: [{
            label: "nsyll",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[0]
        },{
            label: "nsyll-line",
            data: praatData[0],
            type: 'line'
        }]
    };

    var npauseData = {
        labels: names,
        datasets: [{
            label: "npause",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[1]
        },{
            label: "npause-line",
            data: praatData[1],
            type: 'line'
        }]
    };

    var phonationtimeData = {
        labels: names,
        datasets: [{
            label: "phonationtime",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[3]
        },{
            label: "phonationtime-line",
            data: praatData[3],
            type: 'line'
        }]
    };

    var speechrateData = {
        labels: names,
        datasets: [{
            label: "speechrate",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[4]
        },{
            label: "speechrate-line",
            data: praatData[4],
            type: 'line'
        }]
    };

    var articulationData = {
        labels: names,
        datasets: [{
            label: "articulation rate",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[5]
        },{
            label: "articulation rate-line",
            data: praatData[5],
            type: 'line'
        }]
    };

    var ASDData = {
        labels: names,
        datasets: [{
            label: "ASD",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: praatData[6]
        },{
            label: "ASD-line",
            data: praatData[6],
            type: 'line'
        }]
    };



    /****************************
     * PLOT-TIME DATA OPENSMILE *
     ****************************/
    var var1Data = {
        labels: names,
        datasets: [{
            label: "var 1",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[0]
        },{
            label: "nsyll",
            data: openSmileData[0],
            type: 'line'
        }]
    };

    var var2Data = {
        labels: names,
        datasets: [{
            label: "var 2",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[2]
        },{
            label: "nsyll",
            data: openSmileData[2],
            type: 'line'
        }]
    };

    var var3Data = {
        labels: names,
        datasets: [{
            label: "var 3",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[3]
        },{
            label: "nsyll",
            data: openSmileData[3],
            type: 'line'
        }]
    };

    var var4Data = {
        labels: names,
        datasets: [{
            label: "var 4",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[4]
        },{
            label: "nsyll",
            data: openSmileData[4],
            type: 'line'
        }]
    };

    var var5Data = {
        labels: names,
        datasets: [{
            label: "var 5",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[5]
        },{
            label: "nsyll",
            data: openSmileData[5],
            type: 'line'
        }]
    };

    var var6Data = {
        labels: names,
        datasets: [{
            label: "var 6",
            backgroundColor: randomColor,
            borderColor: [
                color[0].replace("0.2","1"),
                color[1].replace("0.2","1"),
                color[2].replace("0.2","1"),
                color[3].replace("0.2","1")
            ],
            borderWidth: 2,
            padding: 10,
            data: openSmileData[6]
        },{
            label: "nsyll",
            data: openSmileData[6],
            type: 'line'
        }]
    };



    /***********************
     * PLOT-BOX DATA PRAAT *
     ***********************/
    var nsyllBox = {
        // define label tree
        labels: ['nsyll'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[0]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[0]]
        }]
    };

    var npauseBox = {
        // define label tree
        labels: ['npause'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[1]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[1]]
        }]
    };

    var phonationBox = {
        // define label tree
        labels: ['phonationtime'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[3]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[3]]
        }]
    };

    var speechrateBox = {
        // define label tree
        labels: ['speechrate'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[4]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[4]]
        }]
    };

    var articulationBox = {
        // define label tree
        labels: ['articulation'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[5]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[5]]
        }]
    };

    var ASDBox = {
        // define label tree
        labels: ['ASD'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 2,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatData[6]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [praatControlData[6]]
        }]
    };


    /***************************
     * PLOT-BOX DATA OPENSMILE *
     ***************************/
    var var1Box = {
        // define label tree
        labels: ['VoicedSegmentsPerSec'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[0]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[0]]
        }]
    };

    var var2Box = {
        // define label tree
        labels: ['slopeUV5001500 sma3nz amean'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[1]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[1]]
        }]
    };

    var var3Box = {
        // define label tree
        labels: ['MeanUnvoicedSegmentLength'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[2]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[2]]
        }]
    };

    var var4Box = {
        // define label tree
        labels: ['F2bandwidth sma3nz stddevNorm'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[3]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[3]]
        }]
    };

    var var5Box = {
        // define label tree
        labels: ['spectralFlux sma3 stddevNorm'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[4]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[4]]
        }]
    };

    var var6Box = {
        // define label tree
        labels: ['loudness sma3 stddevNorm'],
        datasets: [{
            label: 'Diagnosed',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.redback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileData[5]]
        }, {
            label: 'Control',
            backgroundColor:  window.chartColors.blue,
            borderColor: window.chartColors.blueback,
            borderWidth: 1,
            outlierColor: '#999999',
            padding: 10,
            itemRadius: 0,
            data: [openSmileControlData[5]]
        }]
    };







    /*************************
     * RENDER BARPLOTS PRAAT *
     *************************/
    var bar = document.getElementById("nsyll").getContext("2d");
    window.myBar = new Chart(bar,{
        type: 'bar',
        data: nsyllData,
        options: barOptionsPraat
    });

    var bar2 = document.getElementById("npause").getContext("2d");
    window.myBar = new Chart(bar2,{
        type: 'bar',
        data: npauseData,
        options:barOptionsPraat
    });

    var bar3 = document.getElementById("phonationtime").getContext("2d");
    window.myBar = new Chart(bar3,{
        type: 'bar',
        data: phonationtimeData,
        options:barOptionsPraat
    });

    var bar4 = document.getElementById("speechrate").getContext("2d");
    window.myBar = new Chart(bar4,{
        type: 'bar',
        data: speechrateData,
        options:barOptionsPraat
    });

    var bar5 = document.getElementById("articulation").getContext("2d");
    window.myBar = new Chart(bar5,{
        type: 'bar',
        data: articulationData,
        options:barOptionsPraat
    });

    var bar6 = document.getElementById("ASD").getContext("2d");
    window.myBar = new Chart(bar6,{
        type: 'bar',
        data: ASDData,
        options:barOptionsPraat
    });




    /*****************************
     * RENDER BARPLOTS OPENSMILE *
     *****************************/
    var bar8 = document.getElementById("var1").getContext("2d");
    window.myBar = new Chart(bar8,{
        type: 'bar',
        data: var1Data,
        options: barOptionsOpenSmile
    });

    var bar9 = document.getElementById("var2").getContext("2d");
    window.myBar = new Chart(bar9,{
        type: 'bar',
        data: var2Data,
        options: barOptionsOpenSmile
    });

    var bar10 = document.getElementById("var3").getContext("2d");
    window.myBar = new Chart(bar10,{
        type: 'bar',
        data: var3Data,
        options: barOptionsOpenSmile
    });

    var bar11 = document.getElementById("var4").getContext("2d");
    window.myBar = new Chart(bar11,{
        type: 'bar',
        data: var4Data,
        options: barOptionsOpenSmile
    });

    var bar12 = document.getElementById("var5").getContext("2d");
    window.myBar = new Chart(bar12,{
        type: 'bar',
        data: var5Data,
        options: barOptionsOpenSmile
    });

    var bar13 = document.getElementById("var6").getContext("2d");
    window.myBar = new Chart(bar13,{
        type: 'bar',
        data: var6Data,
        options: barOptionsOpenSmile
    });




    /*************************
     * RENDER BOXPLOTS PRAAT *
     *************************/
    var box1 = document.getElementById("boxnsyll").getContext("2d");
    window.myBar = new Chart(box1, {
        type: 'boxplot',
        data: nsyllBox,
        options: boxOptionsPraat
    });

    var box2 = document.getElementById("boxnpause").getContext("2d");
    window.myBar = new Chart(box2, {
        type: 'boxplot',
        data: npauseBox,
        options: boxOptionsPraat
    });


    var box3 = document.getElementById("boxphonation").getContext("2d");
    window.myBar = new Chart(box3, {
        type: 'boxplot',
        data: phonationBox,
        options: boxOptionsPraat
    });

    var box4 = document.getElementById("boxspeech").getContext("2d");
    window.myBar = new Chart(box4, {
        type: 'boxplot',
        data: speechrateBox,
        options: boxOptionsPraat
    });

    var box5 = document.getElementById("boxarticulation").getContext("2d");
    window.myBar = new Chart(box5, {
        type: 'boxplot',
        data: articulationBox,
        options: boxOptionsPraat
    });

    var box6 = document.getElementById("boxasd").getContext("2d");
    window.myBar = new Chart(box6, {
        type: 'boxplot',
        data: ASDBox,
        options: boxOptionsPraat
    });





    /*****************************
     * RENDER BOXPLOTS OPENSMILE *
     *****************************/
    var box7 = document.getElementById("boxvar1").getContext("2d");
    window.myBar = new Chart(box7, {
        type: 'boxplot',
        data: var1Box,
        options: boxOptionsOpenSmile
    });

    var box8 = document.getElementById("boxvar2").getContext("2d");
    window.myBar = new Chart(box8, {
        type: 'boxplot',
        data: var2Box,
        options: boxOptionsOpenSmile
    });

    var box9 = document.getElementById("boxvar3").getContext("2d");
    window.myBar = new Chart(box9, {
        type: 'boxplot',
        data: var3Box,
        options: boxOptionsOpenSmile
    });

    var box11 = document.getElementById("boxvar4").getContext("2d");
    window.myBar = new Chart(box11, {
        type: 'boxplot',
        data: var4Box,
        options: boxOptionsOpenSmile
    });

    var box12 = document.getElementById("boxvar5").getContext("2d");
    window.myBar = new Chart(box12, {
        type: 'boxplot',
        data: var5Box,
        options: boxOptionsOpenSmile
    });

    var box13 = document.getElementById("boxvar6").getContext("2d");
    window.myBar = new Chart(box13, {
        type: 'boxplot',
        data: var6Box,
        options: boxOptionsOpenSmile
    });



};
