// Copyright 2020 Marcel Zandberg

var dataobj;

var randomHash = "";

$(document).ready( function () {

    // Clear the dataobject on load
    dataobj = null;

    //options variables
    var date = "";

    var age = "";

    var genderf = "";

    var genderm = "";

    var degree = "";

    var patid = "";

    var diagnosis = "";



    /***************
     * RANDOM HASH *
     ***************/
    // initalize a new variable that will contain the randomhash string
    var text = "";
    // Store all the allowed characters in a string
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    // Repeat 15 times, add a random character to the string
    for (var i = 0; i < 15; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    // Return the random hash
    randomHash = text;



    /***********
     * ACTIONS *
     ***********/

    // Set the select input of the page # shown to 1
    $("#pageSelector").val("1");

    //opens options uppon click
    $('.options > .open-opt').click(function(){
        $(this).parent().find('.toggle').toggleClass('toggled');
        $(this).parent().find('.expands').toggleClass('open');
    });

    $('.toggle > > .open-opt').click(function(){
        $(this).parent().find('.toggle').toggleClass('toggled');
        $(this).parent().find('.expands').toggleClass('open');
    });


    //get age value
    $('input[name="agefilter"]').blur( function () {
        age = $('input[name="agefilter"]').val();
        $("#selected_age").text(age);
    });

    //get value checked checkbox female
    $('input[name="genderf"]').change( function () {
        if($('#genderf').is(":checked")){
            genderf = $('input[name="genderf"]').val();
        }else{
            genderf = "";
        }
    });

    //get value checked checkbox female
    $('input[name="genderm"]').change( function () {
        if($('#genderm').is(":checked")){
            genderm = $('input[name="genderm"]').val();
        }else{
            genderm = "";
        }
    });

    //show results by one or all on user input
    $('#checkbox-result').click( function() {

        if($('#checkbox-result').hasClass("checked")){
            getAllResultsByOne(date, age, genderf, genderm, degree, patid, diagnosis)
        }else {
            getAllResults(date, age, genderf, genderm, degree, patid, diagnosis)
        }
    });

    //get degree value
    $('#selected-niveau').change( function () {
        degree = $('#selected-niveau').val();
        $("#selected_degree").text(degree);
    });

    //get diagnosis value
    $('#selected-diagnoses').change( function () {
        diagnosis = $('#selected-diagnoses').val();
        $("#selected_diagnosis").text(diagnosis);
    });


    // Init the datepicker
    $('input[name="datefilter"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    // Event handler for the date picker when clicked on the apply button
    $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        date = $('input[name="datefilter"]').val();
        $("#selected_date").text(date);
    });



    // Event handler for the date picker when clicked on the cancel button
    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        date = $('input[name="datefilter"]').val();
    });



    //check | uncheck(all) on click in checkbox
    $('#select-all').click( function () {
        if ($(this).prop('checked')) {
            selectAllListedCheckboxes();
        } else {
            unselectAllListedCheckboxes();
        }
    });



    // Run the filterDelegator function when the user has typed a new character in the input field for the patient ID
    $('input[name="patidfilter"]').keyup( function () {
        patid = $('input[name="patidfilter"]').val();
        getAllResults(date, age, genderf, genderm, degree, patid, diagnosis)

    });

    //get all pat results with given options(can be null) on continue btn
    $("#introtab").click( function () {
        getAllResults(date, age, genderf, genderm, degree, patid, diagnosis);
    });



});

// Event handler for when the user clicks on the view symbol of a result
// "Normal" $.click() doesn't work since the elements are added after the document is loaded
$(document).on('click', '.result-row > .view', function(e) {
    //fire warning before event

    var fileID = $(this).attr("data-file-id");

    var rowID = "#row"+fileID;

    Swal.fire({
        title: 'Are you sure you want to delete this audio file?',
        text: "You won't be able to revert it",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4cd137',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Delete it',
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        footer: '<p class="copy">Timestamp: ' + new Date() + '</p>',
        allowOutsideClick: false
    }).then(function (value) {

        if(value){
            var url = encodeURIComponent("/api/v1/visuals/delete/audio?id="+fileID);
            $.get( "/api?url="+url ).done(function( data ) {
                //remove list element from list
                if (data === 3){
                    $(rowID).hide("slow");
                }else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        footer: '<a href=>Why do I have this issue?</a>'
                    })
                }
            });
        }
    });
});


/*****************************
 * SELECT-ALL BOXES FUNCTION *
 *****************************/

// Function that should set each checkbox to the selected state
function selectAllListedCheckboxes() {
    $( ".result-checkbox-intro" ).each(function() {
        $(this).attr('checked', '').prop('checked', true);
    });
}

// Function that should set each checkbox to the unselected state
function unselectAllListedCheckboxes() {
    $( ".result-checkbox-intro" ).each(function() {
        $(this).removeAttr('checked').prop('checked', false);
    });
}




/*******************
 * RENDER FUNCTIONS *
 *******************/

// Function that will render the results based on the data JSON object it recieves as param
function renderResults(data) {

    // "Back up" the dataobj in a new variable
    dataobj = data;


    // Empty the list with tiems so we don't get duplicates shown
    $("#items-list").empty();

    // Check if there is even any data. If not, show the bummer message. If so, hide it.
    if (data.length < 1) {
        $("#bummer").show();
        return;
    } else {
        $("#bummer").hide();
    }

    //reders de results in a table like format
    $("#allPerPageOpt").attr('value', data.length);


    for (var i = 0; i < data.length; i++) {
            if (i < data.length) {
                var result = data[i];
                var rowdata = '<li class="result-row no_selection" data-result-id="' + result.id + '" id="'+"row"+result.lowest_clipping_id+'"><div class="checker patidchecker"><input type="checkbox" class="result-checkbox-intro" id="'+"check"+i.toString()+'" data-patid="'+result.lowest_clipping_id+'" data-patnr="'+result.patientNumber+'" data-channel="'+result.channel+'"></div><div id="'+i.toString()+'" class="neededid"><div class="name">' + result.patientNumber + '</div><div class="date">' + result.added_at + '</div><div class="name">' + result.sex + '</div><div class="name">' + result.degree + '</div><div class="name">' + result.birthdate + '</div><div class="name">' + result.diag_group + '</div><div class="name">' + result.added_by + '</div></div>';

                var endofrow = '<div class="view view-result" data-file-id="'+result.lowest_clipping_id+'"><i class="fas fa-trash"></i></div></li>';
                $("#items-list").append(rowdata + endofrow);
            } else {
                return;
            }
        }

}



function renderPageSelect(data) {

    $("#pageSelector").empty();
    $("#amountOfResults").text(data.length);

    var numPerPage = $("#numPerPage").val();

    var nOfPages = Math.ceil((data.length/numPerPage))+1;

    // Should return 21 pages

    $("#numOfPages").text(nOfPages-1);

    for (var i = 1; i < nOfPages; i++) {
        $("#pageSelector").append('<option value="'+Number(i-1)+'">'+i+'</option>');
    }
}





/*******************
 * FETCH FUNCTIONS *
 *******************/

//gets the results from the api db and calls the render function
function getAllResults(date, age, genderf, genderm, degree, patid, diagnosis) {

    var startdate = date.split(" - ")[0];
    var enddate = date.split(" - ")[1];

    if(age == ""){
        age = "1-120"
    }

    var startage = age.split("-")[0];
    var endage = age.split("-")[1];

    var gender = genderf + genderm;

    if(gender == ""){
        $("#selected_gender").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>');
    }else {
        $("#selected_gender").text(gender);
    }

    if(gender.length >1){
        gender = "";
    }


    var url = encodeURIComponent("/api/v1/visuals/praat/get/all?startdate="+startdate+"&enddate="+enddate+"&startage="+startage+"&endage="+endage+"&gender="+gender+"&degree="+degree+"&patid="+patid+"&diagnosis="+diagnosis);
    $.get( "/api?url="+url ).done(function( data ) {
        renderResults(data);
        renderPageSelect(data);
    });

}

//gets the results from the api db by one and calls the render function
function getAllResultsByOne(date, age, genderf, genderm, degree, patid, diagnosis) {
    var startdate = date.split(" - ")[0];
    var enddate = date.split(" - ")[1];

    if(age == ""){
        age = "1-120"
    }

    var startage = age.split("-")[0];
    var endage = age.split("-")[1];

    var gender = genderf+genderm;

    if(gender == ""){
        $("#selected_gender").html('<i class="fas fa-times-circle dogwood_blossom_red_text"></i>');
    }else {
        $("#selected_gender").text(gender);
    }

    if(gender.length >1){
        gender = "";
    }

    var url = encodeURIComponent("/api/v1/visuals/praat/get/one?startdate="+startdate+"&enddate="+enddate+"&startage="+startage+"&endage="+endage+"&gender="+gender+"&degree="+degree+"&patid="+patid+"&diagnosis="+diagnosis);
    $.get( "/api?url="+url ).done(function( data ) {
        renderResults(data);
        renderPageSelect(data);
    });

}

