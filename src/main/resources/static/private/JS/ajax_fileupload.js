/**
 * Created by Bas Kasemir.
 */

$( document ).ready( function() {

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    var myFormData = new FormData();

    $("#dropbox").on("dragover", function( event ){
        event.preventDefault();
    });

    $("#dropbox").on("dragenter", function( event ){
        event.preventDefault();
    });
    $("#dropbox").on("dragleave", function( event ){
        event.preventDefault();
    });

    $("#dropbox").on("drop", function() {
        event.preventDefault();

        var files = event.target.files || event.dataTransfer.files;

        //Receive file(s) as objects
        for (var i = 0; i < files.length; i++) {
            //Append to a form
            myFormData.append("files[]", files[i]);
        }
    });


    $("#uploadButton").click( function(e) {

        e.preventDefault();

        /*var ins = $("#files").files.length;
        for (var x = 0; x < ins; x++) {
            myFormData.append("files[]", document.getElementById('files').files[x]);
        }*/

        myFormData.append("patid", "1234");

        var xhr = new XMLHttpRequest;
        xhr.open('POST', '/uploadForm', true);
        xhr.setRequestHeader(header, token);
        xhr.send(myFormData);
    });




});