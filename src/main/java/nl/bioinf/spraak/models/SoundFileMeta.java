package nl.bioinf.spraak.models;

/**
 *  created by Bas Kasemir
 */

import java.sql.Blob;

public class SoundFileMeta {

    private int id;
    private String patient_number;
    private String channel;
    private int lowest_clipping_id;
    private int soundfile_id_1;
    private int soundfile_id_2;
    private String added_at;
    private String added_by;
    private String interviewer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPatient_number() {
        return patient_number;
    }

    public void setPatient_number(String patient_number) {
        this.patient_number = patient_number;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public int getLowest_clipping_id() {
        return lowest_clipping_id;
    }

    public void setLowest_clipping_id(int lowest_clipping_id) {
        this.lowest_clipping_id = lowest_clipping_id;
    }

    public int getSoundfile_id_1() {
        return soundfile_id_1;
    }

    public void setSoundfile_id_1(int soundfile_id_1) {
        this.soundfile_id_1 = soundfile_id_1;
    }

    public int getSoundfile_id_2() {
        return soundfile_id_2;
    }

    public void setSoundfile_id_2(int soundfile_id_2) {
        this.soundfile_id_2 = soundfile_id_2;
    }

    public String getAdded_at() {
        return added_at;
    }

    public void setAdded_at(String added_at) {
        this.added_at = added_at;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }

    public SoundFileMeta(int id, String patient_number, String channel, int lowest_clipping_id, int soundfile_id_1, int soundfile_id_2, String added_at, String added_by, String interviewer) {
        this.id = id;
        this.patient_number = patient_number;
        this.channel = channel;
        this.lowest_clipping_id = lowest_clipping_id;
        this.soundfile_id_1 = soundfile_id_1;
        this.soundfile_id_2 = soundfile_id_2;
        this.added_at = added_at;
        this.added_by = added_by;
        this.interviewer = interviewer;
    }

    public SoundFileMeta(){

    }
}