// Copyright 2019 Bas Kasemir

// Allow the user to skip the introduction (first section, number zero)
var maxSectionNum = 1;
var mayGoFuther = true;

var curr_form_progress = 0;

var num_of_steps = 0;
var step_ids = [];
var progress = 1;

// This boolean will prevent any new animation to be invoked when the current animation is not completed yet and
// does not mesh up the current animation. Therefore this boolean must be set to true so that a animation could
// occur on the page load.
var anim_completed = true;

// Animation speeds in ms
var anim_speed_prev = 750; // Sets the animation speed for fading the current section
var anim_speed_next = 750; // Sets the animation speed for fading the next section
var anim_fade_delay = 500; // Sets the delay between the two animations mentioned above
var anim_completed_time = anim_speed_next+anim_speed_prev-anim_fade_delay;

$( document ).ready(function () {
    // On page load, the user will start at the beginning so set up the variables.
     // System calculation

    // Iterate over each element that has the form-step class
    $( ".form-step" ).each(function( index ) {
        // Update the main variable that holds the number of the total steps
        num_of_steps = index;
        // Add the data of the ID attribute to the array
        step_ids.push($( this ).attr("id"));

        // Get the section name (filled as a locale by thymeleaf)
        var section_name = $( this ).attr("data-section-name");

        // To keep a logic order of shortcuts, we will not bind the 0 (zero) key to a section. Because the section index
        // numbers start with zero, we can only bind a shortcut to the first eight sections. We will set a title
        // containing the section name to each section, and add the specific shortcut if there is one available.
        if (index < 8) {
            var title = section_name + ' | shortcut: shift ⇧ + '+(index+1);
        } else {
            var title = section_name;
        }

        // Lastly, we will appeend the section title with a clickable div to the section header.
        $(".form-progress").append('<div class="section" title="'+title+'" data-section-index="'+index+'">'+ section_name +'</div>');

    });

    $('.isRequiredField').keyup( function () {
        var value = $(this).val();

        if (value === '') {
            mayGoFuther = false;
            var newMax = $(this).next('data-setMaxSection').attr('data-setMaxSection');
            maxSectionNum = newMax;
        } else {
            mayGoFuther = true;
        }
    });

    // When clicked on a element that has the 'data-setMaxSection' attribute, set the value of that attribute to the
    // maximum section the user may go to.
    $('[data-setMaxSection]').click( function () {
        var newMax = $(this).attr('data-setMaxSection');
        maxSectionNum = newMax;
    });

    // Set the length of the progressbar
    $(".form-progress .section").css("width", (1/(num_of_steps+1))*100+'%');
    progress = ((curr_form_progress+1)/(num_of_steps+1))*100;
    $(".form-progress-bar div").css('width', progress-(((1/(num_of_steps+1))*100)*0.5)+'%');

    // Event handler for every keydown event that is preformed on the page
    $(window).keydown( function(event) {
        // Check if the (previous) animation is completed, so there won't be double / glitchy animations.
        if (anim_completed) {
            return;
        }
        // Check if the keypress involves a shift key
        if (event.shiftKey) {
            // Check if the keypress involves a return key. If so, let the user go to the previous section.
            if (event.keyCode === 13) {
                showByNumber(curr_form_progress-1);
            }
            // Check if the keypress involves a number key and if it is in range 1 to 8. If so, let the user go to the
            // the corresponding section.
            if (event.keyCode >= 48 && event.keyCode <= (49+num_of_steps)) {
                showByNumber(String.fromCharCode(event.keyCode-1));
            }
        } else {
            // If only a return key or a 'arrow to the right' key is pressed, go to the next section.
            if (event.keyCode === 13 || event.keyCode === 39) {
                showByNumber(curr_form_progress+1);
            }
            // If only a 'arrow to the left' key is pressed, go to the previous section.
            if (event.keyCode === 37) {
                showByNumber(curr_form_progress-1);
            }
        }
    });

    // If a button with the "next-form-step" class is pressed, go to the next section.
    $(".next-form-step").click( function () {
        showByNumber(curr_form_progress+1);
    });

    // If a button with the "previous-form-step" class is pressed, go to the previous section.
    $(".previous-form-step").click( function () {
        showByNumber(curr_form_progress-1);
    });

    // Event handler for a click on a item in the top / progress header
    $("*[data-section-index]").click( function () {
        // Show the section that corresponds to the number as statet in the custom data attribute
        showByNumber( $(this).attr('data-section-index') );
    });

    function showByNumber( number ) {

        // Check if the requested number is outside of the available sections. If so, do nothing.
        //curr_form_progress >= num_of_steps
        if (number < 0 || number > num_of_steps) {
            return;
        }

        // If the user is not allowed to view this section yet, show an alert.
        if ( number > maxSectionNum) {
            // Timeout is required to work with the return statement. Otherwise the alert would disappear immediately.
            setTimeout(function() {
                Swal.fire({
                    type: 'warning',
                    title: analyse.complete_section,
                    text: analyse.complete_section_msg,
                    showCancelButton: false,
                    footer: '<p class="copy">'+analyse.self_closing+'</p>',
                    timer: 5000
                });
            },50);
            return;
        }

        // If the user is not allowed to go futher yet, show an alert.
        if (!mayGoFuther) {
            // Timeout is required to work with the return statement. Otherwise the alert would disappear immediately.
            setTimeout(function() {
                Swal.fire({
                    type: 'warning',
                    title: analyse.complete_section,
                    text: analyse.complete_section_msg,
                    showCancelButton: false,
                    footer: '<p class="copy">'+analyse.self_closing+'</p>',
                    timer: 5000
                });
            },50);
            return;
        }

        // Set the animation_completed boolean to false so that a next invoked animation wouldn't occur and mesh up the
        // current animation
        anim_completed = false;

        // Construct a selector for the next section div
        var next_form_step = number;
        var selectorn = "#" + step_ids[next_form_step];

        if ( number > curr_form_progress ) {
            // Set the direction and amount of pixels to travel to the top in the animation
            // Since this value is relative negative, it will look like the user goes down a cylinder.
            var topvalue = '-=250px';

        } else if ( number < curr_form_progress ) {
            // Set the direction and amount of pixels to travel to the bottom in the animation
            // Since this value is relative positive, it will look like the user goes up a cylinder.
            var topvalue = '+=250px';
            $(selectorn).css('top', '-45px');

        } else {
            // If the current view should be shown, complete the animation
            anim_completed = true;
            return
        }

        // Construct a selector for the current section div
        var selectorc = "#"+step_ids[curr_form_progress];

        // Animate the current div
        $( selectorc ).animate({
            top: topvalue,
            opacity: 0
        },{
            duration: anim_speed_prev,
            easing: 'easeInOutQuad'
        });

        // Animate the next section div with the delay as defined in the top of this document. Since this is the last
        // animation, the progress should be updated as well after this animation is completed.
        setTimeout( function () {
            $(selectorn).show().animate({
                top: '205px',
                opacity: 1
            }, {
                duration: anim_speed_next,
                easing: 'easeInOutQuad'
            });
            curr_form_progress = Number(number);

            progress = ((curr_form_progress+1)/(num_of_steps+1))*100;
            $(".form-progress-bar div").css('width', progress-(((1/(num_of_steps+1))*100)*0.5)+'%');

            $(selectorn).children(".question").hide();
            $(selectorn + " > .question").eq(0).show();

            setTimeout( function () {
                anim_completed = true;
                $( selectorc ).hide();
            }, anim_completed_time);

        }, anim_fade_delay);

    }

});