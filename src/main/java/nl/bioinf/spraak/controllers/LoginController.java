package nl.bioinf.spraak.controllers;

/**
 *  created by Bas Kasemir
 */

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Login controller serves the login page with security.
 */
@Controller
public class LoginController {
    /**
     * The Logger.
     */
    String className = this.getClass().getSimpleName();
    Logger logger = Logger.getLogger(className);

    /**
     * Greeting string.
     *
     * @param code  the code
     * @param model the model
     * @return the string
     */
    @GetMapping("/login")
    public String greeting(@RequestParam(name="code", required=false, defaultValue="0") String code, Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            logger.log(Level.INFO, "User is already logged in, redirecting to /app");
            /* The user is logged in */
            return "redirect:/app";
        }

        return "login";
    }

}